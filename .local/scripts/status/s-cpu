#!/bin/sh

[ ! -z "$1" ] && BLOCK_BUTTON="$1"

# Cache in tmpfs to improve speed and reduce SSD load
cache=/dev/shm/cpubarscache
widget_icon="💻"

greeter_hogs() {
    ps axch -o cmd:15,%cpu | \
        awk '{a[$1] += $2} END{for (i in a) print i, a[i]"%"}' | \
        sort -rnk2,2 | \
        head
    }

    cpu_bars() {
        # id total idle
        stats=$(awk '/cpu[0-9]+/ {printf "%d %d %d\n", substr($1,4), ($2 + $3 + $4 + $5), $5 }' /proc/stat)
        [ ! -f $cache ] && echo "$stats" > "$cache"
        old=$(cat "$cache")
        echo "$stats" | while read -r row; do
        id=${row%% *}
        rest=${row#* }
        total=${rest%% *}
        idle=${rest##* }

        case "$(echo "$old" | awk '{if ($1 == id)
            printf "%d\n", (1 - (idle - $3)  / (total - $2))*100 /12.5}' \
                id="$id" total="$total" idle="$idle")" in

            "0") printf "▁";;
            "1") printf "▂";;
            "2") printf "▃";;
            "3") printf "▄";;
            "4") printf "▅";;
            "5") printf "▆";;
            "6") printf "▇";;
            "7") printf "█";;
            "8") printf "█";;
        esac
    done

    echo "$stats" > "$cache"
}

case $BLOCK_BUTTON in
    1) notify-send "$widget_icon CPU hogs" "$(greeter_hogs)\\n(100% per core)" ;;
    2) setsid "$TERMINAL" -e htop & ;;
    3) notify-send "$widget_icon CPU module " "
Shows CPU temperature.
- Click to show intensive processes.
- Middle click to open htop." ;;
    6) setsid "$TERMINAL" -e "$EDITOR" "$0" ;;
esac

TOP="$(top -wbn1)"
if [ -n "$TOP" ]; then
    echo "$TOP" | grep "Cpu(s)" | sed "s/.*, *\([0-9.]*\)%* id.*/\1/" | \
        echo "$widget_icon" "$(cpu_bars)" `awk '{printf "%.2d%%\n", 100 - $1}'`
        else
            exit 1
fi
