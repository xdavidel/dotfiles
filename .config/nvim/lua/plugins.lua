local install_packer = function()
    local install_path = vim.fn.stdpath("data") .. '/site/pack/packer/opt/packer.nvim'

    ---@diagnostic disable-next-line: missing-parameter
    if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
        vim.fn.system({
            'git',
            'clone',
            '--depth',
            '1',
            'https://github.com/wbthomason/packer.nvim',
            install_path,
        })
    end
end

function LoadPacker()
    vim.cmd [[packadd packer.nvim]]

    local packer = require('packer')
    packer.init {
        package_root = vim.fn.stdpath("data") .. "/site/pack",
        compile_path = string.format('%s/site/lua/packer_compiled.lua', vim.fn.stdpath('data')),
        display = {
            open_fn = function()
                return require('packer.util').float { border = "single" }
            end,
        },
    }

    packer.startup({
        function(use)
            if not vim.fn.has("nvim-0.8") then
                assert(false, "config require nvim-0.8")
            end

            print("Packer: running statup")
            local packages_dir = 'packages'
            local target = string.format('%s/lua/%s', vim.fn.stdpath ('config'), packages_dir)

            local function noExtension(path) return path:gsub('%.lua$', '') end

            local d = vim.fn.readdir(target, function (n)
                return vim.endswith(n, '.lua')
            end)

            for _,f in ipairs(d) do
                local module = string.format('%s.%s', packages_dir, noExtension(f))
                use(require(module).package)
            end
        end,
    })
end


-- loads everything if suceeded
local has_packer_compiled, _ = pcall(require, 'packer_compiled')

if not has_packer_compiled then
    local has_packer, packer = pcall(require, 'packer')
    if not has_packer then
        install_packer()
    end

    LoadPacker()

    if has_packer then
        packer.compile()
    else
        require('packer').sync()
    end
else

    -- No need to load packer if packer_compiled exists unless requested by
    -- the following commands
    vim.cmd "silent! command PackerClean   lua LoadPacker() require('packer').clean()"
    vim.cmd "silent! command PackerCompile lua LoadPacker() require('packer').compile()"
    vim.cmd "silent! command PackerInstall lua LoadPacker() require('packer').install()"
    vim.cmd "silent! command PackerStatus  lua LoadPacker() require('packer').status()"
    vim.cmd "silent! command PackerSync    lua LoadPacker() require('packer').sync()"
    vim.cmd "silent! command PackerUpdate  lua LoadPacker() require('packer').update()"
    vim.cmd "silent! command PackerProfile lua LoadPacker() require('packer').profile_output()"
end
