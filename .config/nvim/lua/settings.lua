local tab_spaces = 4

local setting = {
    g = {
        mapleader = " ",
    },
    opt = {
        -- Use UTF-8
        fileencoding = "utf-8",

        -- Clipboard On
        clipboard = "unnamedplus",

        -- Incremental live completion
        inccommand = 'nosplit',

        -- Make relative line numbers default
        number = true,
        relativenumber = true,

        -- Do not save when switching buffers
        hidden = true,

        -- Enable mouse mode
        mouse = 'a',

        -- Enable wrap lines indentation
        breakindent = true,

        -- Save undo history
        undofile = true,

        -- Case insensitive searching UNLESS /C or capital in search
        ignorecase = true,
        smartcase = true,

        -- Tabs
        expandtab = true,
        tabstop = tab_spaces,
        softtabstop = tab_spaces,
        shiftwidth = tab_spaces,

        -- Normal Splits
        splitbelow = true,
        splitright = true,

        -- No Wrapping
        wrap = false,

        -- Backups and swap
        swapfile = false,
        backup = false,
        undodir = vim.fn.stdpath 'cache' .. "/undo",
        undofile = true, -- enable persistent undo
        writebackup = false, -- don't allow editing open files

        -- Better color support
        termguicolors = vim.fn.has('termguicolors') == 1,

        -- Show signs in the number column
        signcolumn = "number",

        -- always show completion box and don't preselect
        completeopt = "menuone,noselect",

        -- Limit max completion items
        pumheight = 10,

        -- Decrease update time
        updatetime = 500,

        timeoutlen = 500,

        cursorline = true,

        numberwidth = 4,

        -- start scrolling offset
        scrolloff = 21,

        list = true,

        filetype = "plugin",

        -- Folds
        foldmethod = "indent",
        foldlevel = 100,

        smartindent = true,

        iskeyword = "@,48-57,_,192-255",

        laststatus = 3,

        linebreak = true,

        fillchars = "vert:┃,horiz:━,verthoriz:╋,horizup:┻,horizdown:┳,vertleft:┫,vertright:┣,eob: ",
    },
}

local disable_builtin_plugins = {
    -- "netrw",
    -- "netrwPlugin",
    "netrwSettings",
    "netrwFileHandlers",
    "2html_plugin",
    "getscript",
    "getscriptPlugin",
    "gzip",
    "logipat",
    "matchit",
    "tar",
    "tarPlugin",
    "rrhelper",
    "spellfile_plugin",
    "vimball",
    "vimballPlugin",
    "zip",
    "zipPlugin",
}

-- vim.opt.listchars:append("space:⋅")
-- vim.opt.listchars:append("eol:↴")
-- vim.opt.listchars:append("tab:↹ ")

vim.opt.shortmess:append("sI")

for prefix, tab in pairs(setting) do
    for key, value in pairs(tab) do
        vim[prefix][key] = value
    end
end

for _, builtin_plugin in ipairs(disable_builtin_plugins) do
    vim.g["loaded_" .. builtin_plugin] = 1
end

return setting
