local M = {}

if vim.loop.os_uname().version:match "Windows" then
    M.sep = "\\"
else
    M.sep = "/"
end

M.join = function(...)
    local result = table.concat({ ... }, M.sep)
    return result
end

return M
