local M = {}

if vim.fn.has('win32') > 0 then
    print("Windows")
    M.list_font_families = function()
        if vim.fn.executable('reg') == 1 then
            return {}
        end
        local output = vim.fn.system([[reg query "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts"]])

        -- Remove registry key at start of output.
        local reg_output = vim.call('substitute', output, '.{-}HKEY_LOCAL_MACHINE.{-}\n', '', '')

        -- Remove blank lines.
        local clean_output = vim.call('substitute', reg_output, '\n\n+', '\n', 'g')

        -- Extract font family from each line.  Lines have one of the following
        -- formats; all begin with leading spaces and can have spaces in the
        -- font family portion:
        --   Font family REG_SZ FontFilename
        --   Font family (TrueType) REG_SZ FontFilename
        --   Font family 1,2,3 (TrueType) REG_SZ FontFilename
        -- Throw away everything before and after the font family.
        -- Assume that any '(' is not part of the family name.
        -- Assume digits followed by comma indicates point size.
        local final_output = vim.call('substitute', regOutput, ' *(.{-}) * ((|d+,|REG_SZ) .{-}\n',
        '\1\n', 'g')

        return vim.split(final_output, '\n')
    end
elseif vim.fn.executable('fc-list') == 1 then
    M.list_font_families = function()
        local output = vim.fn.system("fc-list --format '%{family}\n'")
        return vim.split(output, '\n')
    end
else
    M.list_font_families = function()
        return {}
    end
end

M.has_fontfamily = function(family)
    local families = M.list_font_families()
    return vim.tbl_contains(families, family)
end

return M
