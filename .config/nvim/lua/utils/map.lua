local map = {}

function map.register(key_map)
    key_map.options = key_map.options and key_map.options or {}
    key_map.options.desc = key_map.description
    vim.keymap.set(key_map.mode, key_map.lhs, key_map.rhs, key_map.options)
end

function map.unregister(mode, lhs, opts)
    vim.keymap.del(mode, lhs, opts)
end

-- bulk registration shortcuts
function map.bulk_register(group_keymap, opts)
    for _, key_map in pairs(group_keymap) do
        if opts then
            key_map.options = opts
        end
        map.register(key_map)
    end
end

return map
