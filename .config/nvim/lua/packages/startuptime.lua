local M = {
    info = 'profile averaged startup time',
}

--[[
Example collecting 100 samples and using ~/foo.vim as the vimrc script:

:StartupTime ~/foo.vim 100
If -- is found in the command arguments, everything after it will be used verbatim in the program execution.

Example collecting 100 samples with manual arguments:

:StartupTime 100 -- -u ~/foo.vim -i NONE -- ~/foo.vim
Note that the first -- is dropped.
]]

M.package = {
    "tweekmonster/startuptime.vim",
    commit = 'dfa57f522d6f61793fe5fea65bca7484751b8ca2',
    cmd = { "StartupTime" },
}

return M
