local M = {
    info = "Different colors for brackets pairs",
}

M.package = {
    "p00f/nvim-ts-rainbow",
    commit = "fad8badcd9baa4deb2cf2a5376ab412a1ba41797",
    requires = { { "nvim-treesitter/nvim-treesitter" } },
    after = { "nvim-treesitter" },
    event = {"BufRead", "BufNewFile"},
    config = function()
        local has_ts, _ = pcall(require, 'nvim-treesitter')
        if not has_ts then
            return
        end

        require('nvim-treesitter.configs').setup {
            rainbow = {
                enable = true,
                extended_mode = true, -- Also highlight html tags, etc.
                max_file_lines = 1000, -- limit to 1000 lines
            }
        }
    end,
}

return M
