local M = {
    info = 'Extra tools for rust',
}

M.package = {
    'saecki/crates.nvim',
    tag = 'v0.3.0',
    event = { "BufRead Cargo.toml" },
    requires = { { 'nvim-lua/plenary.nvim' } },
    config = function()
        require('crates').setup()

        local has_cmp, cmp = pcall(require, 'cmp')
        if has_cmp then
            cmp.setup.buffer { sources = { { name = 'crates' } } }
        end
    end,
}
return M
