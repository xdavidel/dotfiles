local M = {
    info = "Better File Explorer",
}

M.package = {
    'kyazdani42/nvim-tree.lua',
    commit = 'cf908370fb046641e3aaaa6a6177c1b5d165f186',
    cmd = "NvimTreeToggle",
    setup = function()
        local configs = require('configs')
        configs.plugin.whichkey.mappings.e = { "<cmd>:NvimTreeToggle<CR>", "explorer" }
    end,
    config = function()
        local icons  = require('icons')
        local options = {
            -- enable file highlight for git attributes
            git_hl = 1,

            -- enable folder and file icon highlight for opened files/directories.
            highlight_opened_files = 1,

            -- :help filename-modifiers for more options
            root_folder_modifier = ':~',

            -- append a trailing slash to folder names
            add_trailing = 1,

            -- compact folders that only contain a single folder into one node in the file tree
            group_empty = 1,

            -- used for rendering the space between the icon and the filename.
            icon_padding = ' ',

            -- separator between symlinks' source and target.
            symlink_arrow = ' >> ',

            -- change cwd of nvim-tree to that of new buffer's when opening nvim-tree.
            respect_buf_cwd = 1,

            -- When creating files, sets the path of a file when cursor is on a closed folder
            -- to the parent folder when 0, and inside the folder when 1.
            create_in_closed_folder = 1,

            show_icons = {
                git = 1,
                folders = 1,
                files = 1,
                folder_arrows = 0,
            },
            icons = {
                default = icons.file.default,
                symlink = icons.file.symlink,

                git = {
                    unstaged = icons.git.unstaged,
                    staged = icons.git.staged,
                    unmerged = icons.git.unmerged,
                    renamed = icons.git.renamed,
                    untracked = icons.git.untracked,
                    deleted = icons.git.deleted,
                    ignored = icons.git.deleted,
                },
                folder = {
                    arrow_open = icons.file.folder_open,
                    arrow_closed = icons.file.folder_closed,
                    default = icons.file.folder_closed,
                    open = icons.file.folder_open,
                    empty = icons.file.folder_open,
                    empty_open = icons.file.folder_open,
                    symlink = icons.file.symlink,
                    symlink_open = icons.file.symlink,
                }
            }
        }

        for key, value in pairs(options) do
            vim.g["nvim_tree_" .. key] = value
        end

        local tree_cb = require("nvim-tree.config").nvim_tree_callback
        require'nvim-tree'.setup {
            auto_reload_on_write = true,
            disable_netrw = false,
            hijack_cursor = false,
            hijack_netrw = true,
            hijack_unnamed_buffer_when_opening = false,
            ignore_buffer_on_setup = false,
            open_on_setup = false,
            open_on_setup_file = false,
            open_on_tab = false,
            sort_by = "name",
            update_cwd = false,
            view = {
                width = 30,
                height = 30,
                hide_root_folder = false,
                side = "left",
                preserve_window_proportions = false,
                number = false,
                relativenumber = false,
                signcolumn = "yes",
                mappings = {
                    custom_only = false,
                    list = {
                        { key = { "l", "<CR>", "o" }, cb = tree_cb "edit" },
                        { key = "h", cb = tree_cb "close_node" },
                        { key = "<tab>", cb = tree_cb "open_node" },
                        { key = "v", cb = tree_cb "vsplit" },
                        { key = "t", cb = tree_cb "tabnew" },
                        { key = "<A-CR>", cb = tree_cb "tabnew" },
                    },
                },
            },
            renderer = {
                indent_markers = {
                    enable = false,
                    icons = {
                        corner = "└ ",
                        edge = "│ ",
                        none = "  ",
                    },
                },
                icons = {
                    webdev_colors = true,
                    git_placement = "before",
                }
            },
            hijack_directories = {
                enable = true,
                auto_open = true,
            },
            update_focused_file = {
                enable = false,
                update_cwd = false,
                ignore_list = {},
            },
            ignore_ft_on_setup = {},
            system_open = {
                cmd = "",
                args = {},
            },
            diagnostics = {
                enable = false,
                show_on_dirs = false,
                icons = {
                    hint = icons.sign.exclamation,
                    info = icons.sign.info,
                    warning = icons.sign.warning,
                    error = icons.sign.error,
                },
            },
            filters = {
                dotfiles = false,
                custom = {},
                exclude = {},
            },
            git = {
                enable = true,
                ignore = true,
                timeout = 400,
            },
            actions = {
                use_system_clipboard = true,
                change_dir = {
                    enable = true,
                    global = false,
                    restrict_above_cwd = false,
                },
                open_file = {
                    quit_on_open = false,
                    resize_window = false,
                    window_picker = {
                        enable = true,
                        chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890",
                        exclude = {
                            filetype = { "notify", "packer", "qf", "diff", "fugitive", "fugitiveblame" },
                            buftype = { "nofile", "terminal", "help" },
                        },
                    },
                },
            },
            trash = {
                cmd = "trash",
                require_confirm = true,
            },
            log = {
                enable = false,
                truncate = false,
                types = {
                    all = false,
                    config = false,
                    copy_paste = false,
                    diagnostics = false,
                    git = false,
                    profile = false,
                },
            },
        }
    end,
}

return M
