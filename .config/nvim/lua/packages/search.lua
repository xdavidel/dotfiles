local M = {
    info = 'Better search indications',
}

M.package = {
    'kevinhwang91/nvim-hlslens',
    commit = '7df9c5fffd482821e3cfb3997a5714700d2a5bdd',
    event = {"BufRead", "BufNewFile"},
    config = function()
        local kopts = {noremap = true, silent = true}
        local map = require('utils.map')

        require('hlslens').setup()

        map.bulk_register({
            {
                mode = {'n'},
                lhs = "n",
                rhs = [[<Cmd>execute('normal! ' . v:count1 . 'nzzzv')<CR><Cmd>lua require('hlslens').start()<CR>]],
                options = kopts,
                description = "Next search (center cursor)",
            },
            {
                mode = {'n'},
                lhs = "N",
                rhs = [[<Cmd>execute('normal! ' . v:count1 . 'Nzzzv')<CR><Cmd>lua require('hlslens').start()<CR>]],
                options = kopts,
                description = "Prev search (center cursor)",
            },
            {
                mode = {'n'},
                lhs = "*",
                rhs = [[*<Cmd>lua require('hlslens').start()<CR>]],
                options = kopts,
                description = "Search forward",
            },
            {
                mode = {'n'},
                lhs = "#",
                rhs = [[#<Cmd>lua require('hlslens').start()<CR>]],
                options = kopts,
                description = "Search backward",
            },
            {
                mode = {'n'},
                lhs = "g*",
                rhs = [[g*<Cmd>lua require('hlslens').start()<CR>]],
                options = kopts,
                description = "Search forward",
            },
            {
                mode = {'n'},
                lhs = "g#",
                rhs = [[g#<Cmd>lua require('hlslens').start()<CR>]],
                options = kopts,
                description = "Search backward",
            },
        })

    end,
}

return M
