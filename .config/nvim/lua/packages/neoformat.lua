local M = {
    info = "Global formatting",
}

M.package = {
    "sbdchd/neoformat",
    commit = '1f79f6e6b2a1e2b3ace87d4760769feb4146ff35',
    cmd = "Neoformat",
    config = function()
        -- Format buffer
        vim.cmd [[ command! Format Neoformat ]]

        require('configs').plugin.whichkey.mappings.l.f = { "<cmd>Neoformat<CR>", "Format buffer" }
    end,
}

return M
