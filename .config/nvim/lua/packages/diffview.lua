local M = {
    info = "Better diff view",
}

M.package = {
    "sindrets/diffview.nvim",
    commit = 'ff9ffee2495e8d84fc9deaadaeaf0161be199a30',
    event = "BufRead",
}

return M
