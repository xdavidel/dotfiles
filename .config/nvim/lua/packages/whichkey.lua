local M = {
    info = 'Show your keybindings',
}

M.package = {
    "folke/which-key.nvim",
    commit = '86a58eac6a3bc69f5aa373b29df993d14fda3307',
    config = function()
        local has_whichkey, whichkey = pcall(require, 'which-key')
        if not has_whichkey then
            return
        end

        whichkey.setup()

        vim.cmd(
            [[
        augroup AutoWhichkeyKeys
        autocmd!
        autocmd VimEnter * lua require('packages.whichkey').after()
        ]]
        )
    end,
}

M.after = function()
    local has_whichkey, whichkey = pcall(require, 'which-key')
    if has_whichkey then
        local configs = require('configs')
        local mappings = configs.plugin.whichkey.mappings
        local opts = configs.plugin.whichkey.opts

        local vmappings = configs.plugin.whichkey.vmappings
        local vopts = configs.plugin.whichkey.vopts
        if whichkey['register'] ~= nil then
            whichkey.register(mappings, opts)
            whichkey.register(vmappings, vopts)
        end
    end
end

return M
