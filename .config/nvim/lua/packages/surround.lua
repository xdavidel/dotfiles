local M = {
    info = "Surround text object",
}

M.package = {
    "ur4ltz/surround.nvim",
    commit = '36c253d6470910692491b13382f54c9bab2811e1',
    event = {"BufRead", "BufNewFile"},
    config = function()
        local has_surround, surround = pcall(require, 'surround')
        if has_surround then
            surround.setup {
                mappings_style = "surround"
            }
        end
    end
}

return M
