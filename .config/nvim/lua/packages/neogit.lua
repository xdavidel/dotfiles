local M = {
    info = "Magit implementation for neovim"
}

M.package = {
    "TimUntersberger/neogit",
    cmd = "Neogit",
    commit = 'c1a2a1aa5b4fd774dc26c53f7f256f1320c0ff15',
    setup = function ()
        require('utils.map').register({
            mode = {'n'},
            lhs = '<leader>gs',
            rhs = "<cmd>Neogit<CR>",
            options = { noremap = true, silent = true },
            description = "Git Status",
        })
    end
}

return M
