local M = {
    info = "Better notifications",
}

M.package = {
    "rcarriga/nvim-notify",
    commit = '30e6b5a710319e354c3e362deb7819cb2135c417',
    config = function()
        local has_notify, notify = pcall(require, "notify")
        if not has_notify then
            return
        end

        vim.notify = notify
        local icons = require("icons")

        notify.setup({
            -- Animation style (see below for details)
            stages = "slide",

            -- Function called when a new window is opened, use for changing win settings/config
            on_open = nil,

            -- Function called when a window is closed
            on_close = nil,

            -- Render function for notifications. See notify-render()
            render = "default",

            -- Default timeout for notifications
            timeout = 5000,

            -- Max number of columns for messages
            max_width = nil,
            -- Max number of lines for a message
            max_height = nil,

            -- For stages that change opacity this is treated as the highlight behind the window
            -- Set this to either a highlight group, an RGB hex value e.g. "#000000" or a function returning an RGB code for dynamic values
            background_colour = "#000000",

            -- Minimum width for notification windows
            minimum_width = 50,

            -- Icons for the different levels
            icons = {
                ERROR = icons.sign.error,
                WARN = icons.sign.warning,
                INFO = icons.sign.info,
                DEBUG = icons.general.debug,
                TRACE = icons.general.pencile,
            },
        })

        local has_telescope, telescope = pcall(require, 'telescope')
        if has_telescope then
            telescope.load_extension("notify")
        end
    end
}

return M
