local M = {
    info = "Auto disables search highlighting",
}

M.package = {
    'romainl/vim-cool',
    commit = 'e797f80545073f9e5b0689b91633feda7bb17cc2',
    event = {"BufRead", "BufNewFile"}
}

return M
