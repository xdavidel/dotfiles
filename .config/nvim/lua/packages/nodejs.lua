local M = {
    info = 'Extra nodejs tools',
}

M.package = {
    'David-Kunz/cmp-npm',
    commit = '4b6166c3feeaf8dae162e33ee319dc5880e44a29',
    event = { "BufRead package.json" },
    requires = { { 'nvim-lua/plenary.nvim' } },
    config = function()
        require('cmp-npm').setup()

        local has_cmp, cmp = pcall(require, 'cmp')
        if has_cmp then
            cmp.setup.buffer { sources = { { name = 'npm' } } }
        end
    end,
}
return M
