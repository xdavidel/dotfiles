local M = {
    info = "Fancier statusline",
}

function M.config()
    local has_lualine, lualine = pcall(require, 'lualine')
    if not has_lualine then
        return
    end

    local options = {
        icons_enabled = false,
        theme = 'auto',
        component_separators = {'|', '|'},
        section_separators = {' ', ' '},
        disabled_filetypes = { "dashboard", "NvimTree", "Outline" },
    }

    if vim.fn.has("nvim-0.7.0") == 1 then
        options = vim.tbl_deep_extend("force", options, { globalstatus = true })
    end

    -- globalstatus = true,
    lualine.setup {
        options = options
    }
end

M.package = {
    'hoob3rt/lualine.nvim',
    commit = '9d177b668c18781c53abde92116f141f3d84b04c',
    event = "BufWinEnter",
    config = M.config,
}

return M
