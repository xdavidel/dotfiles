local M = {
    info = "Better project management",
}

M.package = {
    "ahmedkhalf/project.nvim",
    commit = '685bc8e3890d2feb07ccf919522c97f7d33b94e4',
    config = function()
        local has_project_nvim,project_nvim = pcall(require, "project_nvim")
        if not has_project_nvim then
            return
        end

        project_nvim.setup {
            -- When set to false, you will get a message when project.nvim changes your
            -- directory.
            -- silent_chdir = false,
        }

        local has_telescope, telescope = pcall(require, 'telescope')
        if has_telescope then
            telescope.load_extension('projects')
            require('utils.map').register({
                mode = {'n'},
                lhs = '<leader>sp',
                rhs =  "<cmd>Telescope projects<CR>",
                options = { noremap = true, silent = true },
                description = "Projects",
            })
        end
    end
}

return M
