local M = {
    info = "Togglable terminal",
}

M.package = {
    "akinsho/toggleterm.nvim",
    commit = '3ba683827c623affb4d9aa518e97b34db2623093',
    cmd = { "ToggleTerm" },
    setup = function()
        local map = require('utils.map')
        map.bulk_register({
            {
                mode = {'n'},
                lhs = "<C-Space>",
                rhs = [[:ToggleTerm direction=horizontal<CR>]],
                options = { noremap = true, silent = true },
                description = "Toggle horizontal terminal",
            },
            {
                mode = {'i'},
                lhs = "<C-Space>",
                rhs = [[<Esc>:ToggleTerm direction=horizontal<CR>]],
                options = { noremap = true, silent = true },
                description = "Toggle horizontal terminal",
            },
            {
                mode = {'t'},
                lhs = "<C-Space>",
                rhs = [[<C-\><C-n>:ToggleTerm direction=horizontal<CR>]],
                options = { noremap = true, silent = true },
                description = "Toggle horizontal terminal",
            },
            {
                mode = {'n'},
                lhs = [[<C-\>]],
                rhs = [[:ToggleTerm direction=float<CR>]],
                options = { noremap = true, silent = true },
                description = "Toggle floating terminal",
            },
            {
                mode = {'i'},
                lhs = [[<C-\>]],
                rhs = [[<Esc>:ToggleTerm direction=float<CR>]],
                options = { noremap = true, silent = true },
                description = "Toggle floating terminal",
            },
            {
                mode = {'t'},
                lhs = [[<C-\>]],
                rhs = [[<C-\><C-n>:ToggleTerm direction=float<CR>]],
                options = { noremap = true, silent = true },
                description = "Toggle floating terminal",
            },
        })

    end,
    config = function()
        local has_toggleterm, toggleterm = pcall(require, 'toggleterm')
        if not has_toggleterm then
            return
        end

        toggleterm.setup{
            -- open_mapping = [[<C-Space>]],
            size = function (term)
                if term.direction == "horizontal" then
                    return vim.o.lines * 0.8
                elseif term.direction == "vertical" then
                    return vim.o.columns * 0.4
                end
            end
        }
    end,
}

return M
