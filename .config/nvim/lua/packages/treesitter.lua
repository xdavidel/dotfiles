local M = {
    info = 'Highlight, edit, and navigate code using a fast incremental parsing library',
}

M.package = {
    'nvim-treesitter/nvim-treesitter-textobjects',
    commit = '1f1cdc892b9b2f96afb1bddcb49ac1a12b899796',
    event = {"BufRead", "BufNewFile"},
    requires = {
        {
            "nvim-treesitter/nvim-treesitter",
            commit = '2072692aaa4b6da7c354e66c2caf4b0a8f736858',
            event = {"BufRead", "BufNewFile"},
        }
    },
    config = function()
        local has_nvim_treesitter_configs, nvim_treesitter_configs = pcall(require, 'nvim-treesitter.configs')
        if not has_nvim_treesitter_configs then
            return
        end

        nvim_treesitter_configs.setup {
            highlight = {
                enable = true, -- false will disable the whole extension
            },
            incremental_selection = {
                enable = true,
                keymaps = {
                    init_selection = 'gnn',
                    node_incremental = 'grn',
                    scope_incremental = 'grc',
                    node_decremental = 'grm',
                },
            },
            indent = {
                enable = true,
            },
            autopairs = {
                enable = true,
            },
            textobjects = {
                select = {
                    enable = true,
                    lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
                    keymaps = {
                        -- You can use the capture groups defined in textobjects.scm
                        ['af'] = '@function.outer',
                        ['if'] = '@function.inner',
                        ['ac'] = '@class.outer',
                        ['ic'] = '@class.inner',
                    },
                },
                move = {
                    enable = true,
                    set_jumps = true, -- whether to set jumps in the jumplist
                    goto_next_start = {
                        [']m'] = '@function.outer',
                        [']]'] = '@class.outer',
                    },
                    goto_next_end = {
                        [']M'] = '@function.outer',
                        [']['] = '@class.outer',
                    },
                    goto_previous_start = {
                        ['[m'] = '@function.outer',
                        ['[['] = '@class.outer',
                    },
                    goto_previous_end = {
                        ['[M'] = '@function.outer',
                        ['[]'] = '@class.outer',
                    },
                },
            },
        }
    end,
}

return M
