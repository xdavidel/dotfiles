local M = {
    info = 'Debug code in neovim',
}

M.package = {
    "rcarriga/nvim-dap-ui",
    commit = '1e21b3b50e67700e32285b5a74e645311fd8bbea',
    requires = {
        {
            "mfussenegger/nvim-dap",
            commit = '61643680dcb771a29073cd432894e2f81a7c2ae3',
        },
        {
            "theHamsta/nvim-dap-virtual-text",
            commit = '2971ce3e89b1711cc26e27f73d3f854b559a77d4',
        },
        {
            'rcarriga/cmp-dap',
            commit = 'd16f14a210cd28988b97ca8339d504533b7e09a4',
            after = { 'nvim-cmp' },
        },
    },
    config = function ()

        local has_dap, dap = pcall(require, "dap")
        if not has_dap then
            return
        end

        local dubug_adapter_config = {
            go = require("dap.go"),
            python = require("dap.python"),
            dotnet = require("dap.dotnet"),
        }

        for _, dap_options in pairs(dubug_adapter_config) do
            local adapter_name = dap_options.names.adapters
            local configurations_name = dap_options.names.configurations

            dap.adapters[adapter_name] = dap_options.adapters
            dap.configurations[configurations_name] = dap_options.configurations
        end

        local configs = require('configs')

        local map = require("utils.map")

        map.bulk_register({
            {
                mode = {'n'},
                lhs = "<F2>",
                rhs =  ":DapToggleBreakpoint<cr>",
                options = { noremap = true, silent = true },
                description = "Toggle breakpoint",
            },
            {
                mode = {'n'},
                lhs = "<F5>",
                rhs =  ":DapContinue<cr>",
                options = { noremap = true, silent = true },
                description = "continue debug",
            },
            {
                mode = {'n'},
                lhs = "<F6>",
                rhs =  ":DapTerminate<cr>",
                options = { noremap = true, silent = true },
                description = "Stop debugger",
            },
            {
                mode = {'n'},
                lhs = "<F9>",
                rhs =  ":DapStepOut<cr>",
                options = { noremap = true, silent = true },
                description = "step out",
            },
            {
                mode = {'n'},
                lhs = "<F10>",
                rhs = ":DapStepOver<cr>",
                options = { noremap = true, silent = true },
                description = "step over",
            },
            {
                mode = {'n'},
                lhs = "<F11>",
                rhs = ":DapStepInto<cr>",
                options = { noremap = true, silent = true },
                description = "step into",
            },
            {
                mode = {'n'},
                lhs = "<F12>",
                rhs = ":DapToggleRepl<cr>",
                options = { noremap = true, silent = true },
                description = "show debugger repl",
            },
        })

        configs.plugin.whichkey.mappings.d.b = { "<cmd>DapToggleBreakpoint<CR>", "Toggle Breakpoint" }

        local has_dap_ui, dapui = pcall(require, "dapui")
        if not has_dap_ui then
            return
        end

        dapui.setup({
            layouts = {
                {
                    elements = {
                        'scopes',
                        'breakpoints',
                        'stacks',
                        'watches',
                    },
                    size = 40,
                    position = 'left',
                },
                {
                    elements = {
                        'repl',
                        'console',
                    },
                    size = 10,
                    position = 'bottom',
                },
            }
        })

        -- automatically start dapui when debugging starts
        dap.listeners.after.event_initialized["dapui_config"] = function()
            ---@diagnostic disable-next-line: missing-parameter
            dapui.open()
        end
        dap.listeners.before.event_terminated["dapui_config"] = function()
            ---@diagnostic disable-next-line: missing-parameter
            dapui.close()
            dap.repl.close()
        end
        dap.listeners.before.event_exited["dapui_config"] = function()
            ---@diagnostic disable-next-line: missing-parameter
            dapui.close()
            dap.repl.close()
        end

        configs.plugin.whichkey.mappings.d.u = { "<cmd>lua require('dapui').toggle()<CR>", "Toggle UI" }
        configs.plugin.whichkey.mappings.d.e = { "<cmd>lua require('dapui').eval()<CR>", "Execute Debug Expression" }

        local has_virt_text, dap_virt_text = pcall(require, "nvim-dap-virtual-text")
        if not has_virt_text then
            return
        end

        dap_virt_text.setup()

        configs.plugin.whichkey.mappings.d.v = { "<cmd>DapVirtualTextToggle<CR>", "Toggle Virtual Text" }

        local has_cmp, cmp = pcall(require, 'cmp')
        if has_cmp then
            cmp.setup {
                -- nvim-cmp by defaults disables autocomplete for prompt buffers
                enabled = function ()
                    return vim.api.nvim_buf_get_option(0, "buftype") ~= "prompt"
                    or require("cmp_dap").is_dap_buffer()
                end,
                sources = {
                    { name = 'dap' }
                }
            }
        end
    end,
}
return M
