local M = {
    info = "Colorize color names"
}

M.package = {
    "norcalli/nvim-colorizer.lua",
    commit = '36c610a9717cc9ec426a07c8e6bf3b3abcb139d6',
    event = { "BufReadPre" },
    config = function ()
        require('colorizer').setup {
            '*',
            json = { names = false }
        }
    end,
}

return M
