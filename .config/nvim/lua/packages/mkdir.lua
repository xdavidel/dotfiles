local M = {
    info = 'Creating the file path on save',
}

M.package = {
    'jghauser/mkdir.nvim',
    commit = 'c55d1dee4f099528a1853b28bb28caa802eba217',
    event = 'BufWritePre'
}

return M
