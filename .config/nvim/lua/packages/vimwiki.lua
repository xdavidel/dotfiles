local M = {
    info = 'Personal wiki',
}

M.package = {
    'vimwiki/vimwiki',
    ft = "vimwiki.markdown",
    commit = '63af6e72dd3fa840bffb3ebcb8c96970c02e0913',
    cmd = 'VimwikiIndex',
    setup = function()
        local wiki = {
            path = '~/Documents/wiki',
            syntax = 'markdown',
            ext = '.md',
            exclude_files = { '**/README.md', '**/Readme.md' },
        }

        local pwiki = {
            path = '~/Documents/private',
            syntax = 'markdown',
            ext = '.md',
            exclude_files = { '**/README.md', '**/Readme.md' },
        }

        vim.g.vimwiki_filetypes = { 'markdown' }
        vim.g.vimwiki_list = { wiki, pwiki }
        vim.cmd "autocmd BufWritePost *wiki/*md VimwikiTOC"
    end,
}

return M
