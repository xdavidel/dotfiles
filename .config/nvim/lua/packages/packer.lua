local M = {
    info = "Package manager",
}

M.package = {
    'wbthomason/packer.nvim',
    commit = '851c62c5ecd3b5adc91665feda8f977e104162a5',
    opt = true,
}

return M
