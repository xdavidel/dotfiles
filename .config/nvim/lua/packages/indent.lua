local M = {
    info = "Add indentation guides even on blank lines",
}

M.package = {
    'lukas-reineke/indent-blankline.nvim',
    event = {"BufRead", "BufNewFile"},
    commit = 'db7cbcb40cc00fc5d6074d7569fb37197705e7f6',
    config = function ()
        -- Map blankline
        vim.g.indent_blankline_char = '┊'
        vim.g.indent_blankline_filetype_exclude = { 'help', 'packer' }
        vim.g.indent_blankline_buftype_exclude = { 'terminal', 'nofile' }
        vim.g.indent_blankline_char_highlight = 'LineNr'
        vim.g.indent_blankline_show_trailing_blankline_indent = false
    end
}

return M
