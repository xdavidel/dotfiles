local M = {
    info = "Autocompletion"
}

M.package = {
    'hrsh7th/nvim-cmp',
    commit = 'aee40113c2ba3ab158955f233ca083ca9958d6f8',
    event = { "BufEnter" },
    requires = {
        {
            "L3MON4D3/LuaSnip",
            commit = '619796e2477f7233e5fdff456240676a08482684',
            after = { 'nvim-cmp' },
        },
        {
            "hrsh7th/cmp-nvim-lsp",
            commit = '78924d1d677b29b3d1fe429864185341724ee5a2',
            after = { 'nvim-cmp' },
        },
        {
            "saadparwaiz1/cmp_luasnip",
            commit = '18095520391186d634a0045dacaa346291096566',
            after = { 'nvim-cmp' },
        },
        {
            "hrsh7th/cmp-buffer",
            commit = '3022dbc9166796b644a841a02de8dd1cc1d311fa',
            after = { 'nvim-cmp' },
        },
        {
            "f3fora/cmp-spell",
            commit = '60584cb75e5e8bba5a0c9e4c3ab0791e0698bffa',
            after = { 'nvim-cmp' },
        },
        {
            "hrsh7th/cmp-path",
            commit = '91ff86cd9c29299a64f968ebb45846c485725f23',
            after = { 'nvim-cmp' },
        },
        {
            "hrsh7th/cmp-calc",
            commit = 'f7efc20768603bd9f9ae0ed073b1c129f63eb312',
            after = { 'nvim-cmp' },
        },
        {
            "hrsh7th/cmp-cmdline",
            commit = '8bc9c4a34b223888b7ffbe45c4fe39a7bee5b74d',
            after = { 'nvim-cmp' },
        },
        {
            "hrsh7th/cmp-emoji",
            commit = '19075c36d5820253d32e2478b6aaf3734aeaafa0',
            after = { 'nvim-cmp' },
        },
        {
            "kdheepak/cmp-latex-symbols",
            commit = '46e7627afa8c8ff57158d1c29d721d8efebbc39f',
            after = { 'nvim-cmp' },
        },
    },
    config = function()
        -- luasnip setup

        local has_luasnip, luasnip = pcall(require, 'luasnip')
        if not has_luasnip then
            return
        end

        local kind_icons = require('icons').lsp

        -- nvim-cmp setup
        local cmp = require 'cmp'

        local mappings

        if not vim.fn.has("nvim-0.7.0") then
            mappings = {
                ['<C-p>'] = cmp.mapping.select_prev_item(),
                ['<C-n>'] = cmp.mapping.select_next_item(),
                ['<C-d>'] = cmp.mapping.scroll_docs(-4),
                ['<C-f>'] = cmp.mapping.scroll_docs(4),
                ['<C-Space>'] = cmp.mapping.complete(),
                ['<C-e>'] = cmp.mapping.close(),
                ['<CR>'] = cmp.mapping.confirm {
                    behavior = cmp.ConfirmBehavior.Replace,
                    select = false,
                },
                ['<Tab>'] = {
                    behavior = cmp.ConfirmBehavior.Replace,
                    select = false,
                },
            }
        else
            mappings = cmp.mapping.preset.insert({
                ['<C-p>'] = cmp.mapping.select_prev_item(),
                ['<C-n>'] = cmp.mapping.select_next_item(),
                ['<C-b>'] = cmp.mapping.scroll_docs(-4),
                ['<C-f>'] = cmp.mapping.scroll_docs(4),
                ['<C-Space>'] = cmp.mapping.complete(),
                ['<C-e>'] = cmp.mapping.abort(),
                -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
                ['<CR>'] = cmp.mapping.confirm({ select = false }),
                ['<Tab>'] = cmp.mapping.confirm({ select = false }),
            })

        end

        cmp.setup {
            snippet = {
                expand = function(args)
                    luasnip.lsp_expand(args.body)
                end,
            },
            mapping = mappings,
            sorting = {
                priority_weight = 2,
                comparators = {
                    cmp.config.compare.offset,
                    cmp.config.compare.exact,
                    cmp.config.compare.score,
                    cmp.config.compare.kind,
                    cmp.config.compare.sort_text,
                    cmp.config.compare.length,
                    cmp.config.compare.order,
                },
            },
            formatting = {
                fields = { "kind", "abbr", "menu" },
                format = function(entry, vim_item)
                    -- Kind icons
                    vim_item.kind = string.format("%s", kind_icons[vim_item.kind])
                    vim_item.menu = ({
                        nvim_lsp = "[LSP]",
                        luasnip  = "[Snippet]",
                        buffer   = "[Buffer]",
                        spell    = "[Spell]",
                        path     = "[Path]",
                        calc     = "[Calc]",
                    })[entry.source.name]
                    return vim_item
                end,
            },
            sources = {
                { name = 'nvim_lsp' },
                { name = 'luasnip' },
                { name = 'buffer' },
                { name = 'spell' },
                { name = 'path' },
                { name = 'calc' },
                { name = 'emoji' },
                { name = 'latex_symbols' },
            },
            confirm_opts = {
                behavior = cmp.ConfirmBehavior.Replace,
                select = false,
            },
            window = {
                completion = cmp.config.window.bordered({
                    winhighlight = "Normal:None,FloatBorder:FloatBorder,CursorLine:PmenuSel,Search:None",
                }),
                documentation = cmp.config.window.bordered({
                    winhighlight = "Normal:None,FloatBorder:FloatBorder,CursorLine:PmenuSel,Search:None",
                }),
            },
            -- documentation = {
            --   border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" },
            -- },
            experimental = {
                ghost_text = false,
                native_menu = false,
            },
        }

        -- Command line completions
        cmp.setup.cmdline(':', {
            sources = {
                { name = 'path' },
                { name = 'cmdline' }
            }
        })

        cmp.setup.cmdline('/', {
            sources = {
                { name = 'buffer' }
            }
        })

        cmp.setup.cmdline('?', {
            sources = {
                { name = 'buffer' }
            }
        })
    end
}

return M
