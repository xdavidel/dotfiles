local M = {
    info = 'interactive scratchpad for hackers',
}

M.package = {
    "metakirby5/codi.vim",
    commit = '28983696f59f47221380b4f7d78237dc04f9c62f',
    cmd = { "Codi", "CodiNew" },
}

return M
