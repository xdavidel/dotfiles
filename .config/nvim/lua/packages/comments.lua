local M = {
    info = "Smart commants",
}

M.package = {
    'numToStr/Comment.nvim',
    commit = 'ad7ffa8ed2279f1c8a90212c7d3851f9b783a3d6',
    event = "BufWinEnter",
    config = function()
        require('Comment').setup()
    end
}

return M
