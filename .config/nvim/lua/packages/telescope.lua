local M = {
    info = 'UI to select things (files, grep results, open buffers...)',
}

M.package = {
    'nvim-telescope/telescope.nvim',
    commit = '7a4ffef931769c3fe7544214ed7ffde5852653f6',
    requires = {
        {'nvim-lua/plenary.nvim'},
        {
            'nvim-telescope/telescope-file-browser.nvim',
            commit = 'e65a5674986dcaf27c0fd852b73f5fc66fa78181',
        },
    },
    config = function ()
        local has_telescope, telescope = pcall(require, 'telescope')
        if not has_telescope then
            return
        end

        telescope.load_extension('file_browser')

        local actions = require('telescope.actions')
        telescope.setup {
            active = false,
            defaults = {
                prompt_prefix = "> ",
                selection_caret = "* ",
                entry_prefix = "  ",
                initial_mode = "insert",
                selection_strategy = "reset",
                sorting_strategy = "descending",
                layout_strategy = "horizontal",
                layout_config = {
                    width = 0.85,
                    prompt_position = "bottom",
                    preview_cutoff = 120,
                    horizontal = { mirror = false },
                    vertical = { mirror = false },
                },
                file_ignore_patterns = {},
                generic_sorter = require("telescope.sorters").get_generic_fuzzy_sorter,
                path_display = {},
                winblend = 0,
                color_devicons = false,
                border = {},
                borderchars = { "─", "│", "─", "│", "╭", "╮", "╯", "╰" },
                use_less = true,
                set_env = { ["COLORTERM"] = "truecolor" }, -- default = nil,
                file_previewer = require("telescope.previewers").vim_buffer_cat.new,
                grep_previewer = require("telescope.previewers").vim_buffer_vimgrep.new,

                mappings = {
                    i = {
                        ["<C-n>"] = actions.cycle_history_next,
                        ["<C-p>"] = actions.cycle_history_prev,
                        ["<C-c>"] = actions.close,
                        ["<C-j>"] = actions.move_selection_next,
                        ["<C-k>"] = actions.move_selection_previous,
                        ["<C-q>"] = actions.smart_send_to_qflist + actions.open_qflist,
                        ['<C-u>'] = false,
                        ['<C-d>'] = false,
                        ["<CR>"]  = actions.select_default + actions.center,
                        ["<tab>"] = actions.select_default + actions.center,
                    },
                    n = {
                        ["<C-j>"] = actions.move_selection_next,
                        ["<C-k>"] = actions.move_selection_previous,
                        ["<C-q>"] = actions.smart_send_to_qflist + actions.open_qflist,
                    },
                },
            },
        }

        local icons = require('icons')
        local file_browser

        file_browser = telescope.extensions.file_browser.file_browser

        FileExplorer = function()
            local opts = {
                cwd = vim.fn.expand("$HOME"),
                prompt_title = "Explorer",
                hidden = true,
                dir_icon = icons.file.folder_closed,
            }

            file_browser(opts)
        end

        RelativeFiles = function()
            local opts = {
                prompt_title = "Files",
                hidden = true,
                depth = 2,
                dir_icon = icons.file.folder_closed,
            }

            file_browser(opts)
        end


        local map = require("utils.map")
        map.bulk_register({
            {
                mode = {'n'},
                lhs = '<C-p>',
                rhs = [[<cmd>Telescope find_files<cr>]],
                options = { noremap = true, silent = true },
                description = "Find Files",
            },
            {
                mode = {'n'},
                lhs = '<A-x>',
                rhs = [[<cmd>Telescope commands<cr>]],
                options = { noremap = true, silent = true },
                description = "Commands",
            },
        })

        local configs = require('configs')
        configs.plugin.whichkey.mappings.f.f = { "<cmd>lua FileExplorer()<cr>",     "File Explorer" }
        configs.plugin.whichkey.mappings.f.d = { "<cmd>lua RelativeFiles()<cr>",    "Find Files" }
        configs.plugin.whichkey.mappings.b.s = { "<cmd>Telescope buffers<cr>",      "Switch Buffers" }
        configs.plugin.whichkey.mappings.f.r = { "<cmd>Telescope oldfiles<cr>",     "Recent Files" }
        configs.plugin.whichkey.mappings.s.b = { "<cmd>Telescope git_branches<cr>", "Checkout branch" }
        configs.plugin.whichkey.mappings.s.c = { "<cmd>Telescope colorscheme<cr>",  "Colorscheme" }
        configs.plugin.whichkey.mappings.s.f = { "<cmd>Telescope find_files<cr>",   "Find File" }
        configs.plugin.whichkey.mappings.s.h = { "<cmd>Telescope help_tags<cr>",    "Find Help" }
        configs.plugin.whichkey.mappings.s.M = { "<cmd>Telescope man_pages<cr>",    "Man Pages" }
        configs.plugin.whichkey.mappings.s.r = { "<cmd>Telescope oldfiles<cr>",     "Open Recent File" }
        configs.plugin.whichkey.mappings.s.R = { "<cmd>Telescope registers<cr>",    "Registers" }
        configs.plugin.whichkey.mappings.s.t = { "<cmd>Telescope live_grep<cr>",    "Text" }
    end
}

return M
