local M = {
    info = "Add git related info in the signs columns and popups",
}

M.package = {
    'lewis6991/gitsigns.nvim',
    commit = '9ff7dfb051e5104088ff80556203634fc8f8546d',
    requires = { 'nvim-lua/plenary.nvim' },
    event = "BufRead",
    config = function()
        local icons = require('icons')
        require('gitsigns').setup {
            signs = {
                add = {
                    hl = "GitSignsAdd",
                    text = icons.git.add,
                    numhl = "GitSignsAddNr",
                    linehl = "GitSignsAddLn",
                },
                change = {
                    hl = "GitSignsChange",
                    text = icons.git.change,
                    numhl = "GitSignsChangeNr",
                    linehl = "GitSignsChangeLn",
                },
                delete = {
                    hl = "GitSignsDelete",
                    text = icons.git.delete_bottom,
                    numhl = "GitSignsDeleteNr",
                    linehl = "GitSignsDeleteLn",
                },
                topdelete = {
                    hl = "GitSignsDelete",
                    text = icons.git.delete_top,
                    numhl = "GitSignsDeleteNr",
                    linehl = "GitSignsDeleteLn",
                },
                changedelete = {
                    hl = "GitSignsChange",
                    text = icons.git.change_delete,
                    numhl = "GitSignsChangeNr",
                    linehl = "GitSignsChangeLn",
                },
            },
            signcolumn = true,
            numhl      = false,
            linehl     = false,
            word_diff  = false,
            diff_opts = {
                internal = false -- fix issue with CRLF
            }
        }
    end,
}

return M
