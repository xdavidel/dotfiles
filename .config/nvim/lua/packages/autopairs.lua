local M = {
    info = "A super powerful autopair plugin for Neovim that supports multiple characters",
}

M.package = {
    "windwp/nvim-autopairs",
    commit = '6b6e35fc9aca1030a74cc022220bc22ea6c5daf4',
    event = {"InsertEnter"},
    config = function()
        local has_autopairs, autopairs = pcall(require, 'nvim-autopairs')
        if not has_autopairs then
            return
        end

        autopairs.setup{
            check_ts = true,
            ts_config = {
                lua = { "string", "source" },
                javascript = { "string", ")template_string" },
                java = false,
            },
            disable_filetype = { "TelescopePrompt", "spectre_panel" },
            fast_wrap = {
                map = "<M-e>",
                chars = { "{", "[", "(", '"', "'" },
                pattern = string.gsub([[ [%'%"%)%>%]%)%}%,] ]], "%s+", ""),
                offset = 0, -- Offset from pattern match
                end_key = "$",
                keys = "qwertyuiopzxcvbnmasdfghjkl",
                check_comma = true,
                highlight = "PmenuSel",
                highlight_grey = "LineNr",
            },
        }
    end
}

return M
