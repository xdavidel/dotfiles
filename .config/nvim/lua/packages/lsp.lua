local M = {
    info = "Install LSP servers",
}

M.package = {
    'williamboman/mason.nvim',
    commit = 'd85d71e910d1b2c539d17ae0d47dad48f8f3c8a7',
    event = { "BufRead", "BufNewFile" },
    requires = {
        {
            "neovim/nvim-lspconfig",
            commit = 'c3e0af1a734538bd02e12887221ae3186bbe5683',
            event = { "BufRead", "BufNewFile" },
        },
        {
            "williamboman/mason-lspconfig.nvim",
            commit = 'a910b4d50f7a32d2f9057d636418a16843094b7c',
            event = { "BufRead", "BufNewFile" },
        },
        {
            "ray-x/lsp_signature.nvim",
            commit = '12c0807919ae60c7bdae8c0bd0072ee9d752fa1a',
            event = { "BufRead", "BufNewFile" },
        },
        {
            "folke/lua-dev.nvim",
            commit = 'e3b934841be2f03dea0f3d5d0c7668a2bd6d2a5c',
            event = { "BufRead", "BufNewFile" },
        },
    },
    config = function()
        local icons = require("icons")
        local map = require('utils.map')

        local lsp_keymaps = function(bufnr)
            local opts = { noremap = true, silent = true, buffer = bufnr }
            map.bulk_register({
                {
                    mode = { 'n' },
                    lhs = "K",
                    rhs = "<cmd>lua vim.lsp.buf.hover()<CR>",
                    description = "documentation",
                },
                {
                    mode = { 'n' },
                    lhs = "<leader>ca",
                    rhs = "<cmd>lua vim.lsp.buf.code_action()<CR>",
                    description = "code action",
                },
                {
                    mode = { 'v' },
                    lhs = "<leader>ca",
                    rhs = "<cmd>lua vim.lsp.buf.code_action()()<CR>",
                    description = "code action",
                },
                {
                    mode = { 'n' },
                    lhs = "gr",
                    rhs = "<cmd>lua vim.lsp.buf.references()<CR>",
                    description = "definition",
                },
                {
                    mode = { 'n' },
                    lhs = "gd",
                    rhs = "<cmd>lua vim.lsp.buf.definition()<CR>",
                    description = "definition",
                },
                {
                    mode = { 'n' },
                    lhs = "gD",
                    rhs = "<cmd>lua vim.lsp.buf.declaration()<CR>",
                    description = "declaration",
                },
                {
                    mode = { 'n' },
                    lhs = "<c-k>",
                    rhs = "<cmd>lua vim.lsp.buf.signature_help()<CR>",
                    description = "signature help",
                },
                {
                    mode = { 'n' },
                    lhs = "gs",
                    rhs = "<cmd>lua require('telescope.builtin').lsp_document_symbols()<CR>",
                    description = "symbols",
                },
                {
                    mode = { 'n' },
                    lhs = "<leader>gS",
                    rhs = "<cmd>lua vim.lsp.buf.type_definition()<CR>",
                    description = "type definition",
                },
                {
                    mode = { 'n' },
                    lhs = "gl",
                    rhs = "<cmd>lua vim.diagnostic.open_float()<CR>",
                    description = "diagnostics",
                },
                {
                    mode = {'n'},
                    lhs = "[d",
                    rhs = '<cmd>lua vim.diagnostic.goto_prev()<CR>',
                    description = "prev diagnostic",
                },
                {
                    mode = {'n'},
                    lhs = "]d",
                    rhs = '<cmd>lua vim.diagnostic.goto_next()<CR>',
                    description = "next diagnostic",
                },
                {
                    mode = {'n'},
                    lhs = "<C-LeftMouse>",
                    rhs = "<cmd>lua vim.lsp.buf.definition()<CR>",
                    description = "definition",
                },
                {
                    mode = {'n'},
                    lhs = "<leader>lr",
                    rhs = "<cmd>lua vim.lsp.buf.rename()<CR>",
                    description = "rename",
                },
                {
                    mode = {'n'},
                    lhs = "<leader>gi",
                    rhs = "<cmd>lua vim.lsp.buf.incoming_calls()<CR>",
                    description = "incoming calls",
                },
                {
                    mode = {'n'},
                    lhs = "<leader>go",
                    rhs = "<cmd>lua vim.lsp.buf.outgoing_calls()<CR>",
                    description = "outgoing calls",
                },
                {
                    mode = {'n'},
                    lhs = "gI",
                    rhs = "<cmd>lua vim.lsp.buf.implementation()<CR>",
                    description = "implementation",
                },
            }, opts)
        end

        local lsp_highlight_document = function(client)
            vim.api.nvim_exec(
                [[
                augroup lsp_document_highlight
                autocmd! * <buffer>
                autocmd CursorHold  <buffer> lua vim.lsp.buf.document_highlight()
                autocmd CursorHoldI <buffer> lua vim.lsp.buf.document_highlight()
                autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
                augroup END
                ]],
                false
            )
        end

        local on_attach = function(client, bufnr)
            local has_lsp_signature, lsp_signature = pcall(require, "lsp_signature")
            if has_lsp_signature then
                lsp_signature.on_attach({
                    floating_window = true, -- true = has floating window, false = virtual text only
                    hi_parameter = "Search", -- how your parameter will be highlight
                    bind = true, -- This is mandatory, false on lspsaga
                    hint_enable = true, -- virtual hint enable
                    hint_prefix = "",  -- default was Panda for parameter
                    handler_opts = {
                        border = "rounded"   -- double, rounded, single, shadow, none
                    },
                }, bufnr)
            end
            lsp_keymaps(bufnr)
            lsp_highlight_document(client)
        end

        require("mason").setup()
        require("mason-lspconfig").setup()
        require("mason-lspconfig").setup_handlers {
            -- The first entry (without a key) will be the default handler
            -- and will be called for each installed server that doesn't have
            -- a dedicated handler.
            function (server_name) -- default handler (optional)
                require("lspconfig")[server_name].setup {
                    on_attach = on_attach,
                }
            end,
            -- Next, you can provide a dedicated handler for specific servers.
            -- For example, a handler override for the `rust_analyzer`:
        }

        -- -- Globals bindings
        map.bulk_register({
            {
                mode = { 'n' },
                lhs = "<leader>li",
                rhs = "<cmd>LspInfo<CR>",
                options = { noremap = true, silent = true },
                description = "info",
            },
            {
                mode = { 'n' },
                lhs = "<leader>lI",
                rhs = "<cmd>Mason<CR>",
                options = { noremap = true, silent = true },
                description = "manage",
            },
            {
                mode = { 'n' },
                lhs = "<leader>lR",
                rhs = "<cmd>LspRestart<CR>",
                options = { noremap = true, silent = true },
                description = "restart server",
            },
        })

        local signs = {
            { name = "LspDiagnosticsSignError", text = icons.sign.error },
            { name = "LspDiagnosticsSignWarning", text = icons.sign.warning },
            { name = "LspDiagnosticsSignHint", text = icons.sign.exclamation },
            { name = "LspDiagnosticsSignInformation", text = icons.sign.info },
        }

        for _, sign in ipairs(signs) do
            vim.fn.sign_define(sign.name, { texthl = sign.name, text = sign.text, numhl = sign.name })
        end

        -- nvim-cmp supports additional completion capabilities
        local capabilities = vim.lsp.protocol.make_client_capabilities()

        local has_cmp_lsp, cmp_nvim_lsp = pcall(require, 'cmp_nvim_lsp')
        if has_cmp_lsp then
            capabilities = cmp_nvim_lsp.default_capabilities(capabilities)
        end

    end,
}

return M
