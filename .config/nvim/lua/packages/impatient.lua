local M = {
    info = "Speed up loading Lua modules",
}

M.package = {
    'lewis6991/impatient.nvim',
    commit = 'd3dd30ff0b811756e735eb9020609fa315bfbbcc'
}

return M
