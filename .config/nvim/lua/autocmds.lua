local configs = require('configs')

-- Close those dialogs with 'q'
vim.api.nvim_exec(
  [[
  augroup CloseDialogs
    autocmd!
    autocmd FileType qf,help,man,lspinfo,null-ls-info nnoremap <silent> <buffer> q :close<CR>
  augroup end
]],
  false
)

-- Highlight on yank
vim.api.nvim_exec(
  [[
  augroup YankHighlight
    autocmd!
    autocmd TextYankPost * silent! lua vim.highlight.on_yank()
  augroup end
]],
  false
)

-- Auto remove background color
if configs.colors.transparent then
    vim.cmd(
    [[
    augroup Colorscheme
    autocmd!
    autocmd ColorScheme * hi Normal ctermbg=none guibg=none
    augroup end
    ]],
    false
    )
end

-- Restore last cursor position in open
vim.cmd(
  [[
  augroup Startup
    autocmd!
    autocmd BufReadPost * if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit' | exe "normal! g`\"" | endif
  augroup end
]],
  false
)


-- Filetypes update on write
vim.cmd(
  [[
  augroup FtWrite
    autocmd!
    autocmd BufWritePost *Xresources,*Xdefaults !xrdb %
    autocmd BufWritePost *sxhkdrc !pkill -USR1 sxhkd
    autocmd BufWritePost ~/.config/bspwm/bspwmrc !~/.config/bspwm/bspwmrc
  augroup end
]],
  false
)


-- Automatically deletes all trailing whitespace and newlines at end of file on save
vim.cmd(
  [[
  augroup AutoCleanWrite
    autocmd!
    autocmd BufWritePre * let currPos = getpos(".")
    autocmd BufWritePre * %s/\s\+$//e
    autocmd BufWritePre * %s/\n\+\%$//e
    autocmd BufWritePre *.[ch] %s/\%$/\r/e
    autocmd BufWritePre * cal cursor(currPos[1], currPos[2])
  augroup end
]],
  false
)

-- Set scripts to be executable from the shell
vim.cmd(
  [[
  augroup AutoExecutableFile
    autocmd!
    autocmd BufWritePost * if getline(1) =~ "^#!" | if getline(1) =~ "/bin/" | silent execute "!chmod +x <afile>" | endif | endif
  augroup end
]],
  false
)

-- Open xresources files
vim.cmd(
  [[
  augroup XresourcesFiles
    autocmd!
    autocmd BufReadPost,BufNewFile .Xresources,.Xdefaults,xresources,xdefaults set filetype=xresources
  augroup end
]],
  false
)

-- Auto spellcheck
vim.cmd(
  [[
  augroup SpellCheck
    autocmd!
    autocmd FileType markdown,org,gitcommit,txt setlocal spell spelllang=en_us
  augroup end
]],
  false
)

-- Disable auto comments (must be via autocommands)
vim.cmd(
  [[
  augroup AutoComments
    autocmd!
    autocmd InsertEnter * set formatoptions-=cro
  augroup end
]],
  false
)
