local M = {}

M.colors = {
    theme = 'nvcode',
    transparent = true,
}

M.has_fonts = (function()
    return false
end)()

M.plugin = {
    whichkey = {
      opts = {
        mode = "n", -- NORMAL mode
        prefix = "<leader>",
        buffer = nil, -- Global mappings. Specify a buffer number for buffer local mappings
        silent = true, -- use `silent` when creating keymaps
        noremap = true, -- use `noremap` when creating keymaps
        nowait = true, -- use `nowait` when creating keymaps
      },
      vopts = {
        mode = "v", -- NORMAL mode
        prefix = "<leader>",
        buffer = nil, -- Global mappings. Specify a buffer number for buffer local mappings
        silent = true, -- use `silent` when creating keymaps
        noremap = true, -- use `noremap` when creating keymaps
        nowait = true, -- use `nowait` when creating keymaps
      },
      mappings = {
        b = {
          name = "Buffer",
        },
        c = {
          c = { ":w! | !compiler <c-r>%<CR>", "Compile" },
          b = { ":!checkbashisms -xfp %<CR>", "Shellcheck" },
        },
        d = {
          name = "Debug",
        },
        f = {
          name = "File",
        },
        g = { name = "Git / Go" },
        h = { '<cmd>let @/=""<CR>', "No Highlight" },
        l = {
          name = "LSP",
          w = {
            name = "Workspace",
          }
        },
        p = {
          name = "Packer",
          c = { "<cmd>PackerCompile<cr>", "Compile" },
          i = { "<cmd>PackerInstall<cr>", "Install" },
          s = { "<cmd>PackerSync<cr>", "Sync" },
          u = { "<cmd>PackerUpdate<cr>", "Update" },
        },
        s = {
          name = "Search",
        },
        t = {
          name = "Toggle",
          s = { "<cmd>setlocal spell! spelllang=en_us<cr>", "Spell" },
          l = { "<cmd>setlocal relativenumber!<cr>", "Line Numbers" },
        },
        T = {
          name = "Treesitter",
          i = { ":TSConfigInfo<cr>", "Info" },
        },
      },
      vmappings = {
        ["/"] = { ":CommentToggle<CR>", "Comment" },
      }
    }
}

return M
