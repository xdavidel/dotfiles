local M = {}

M.general = {
    bar = "| ",
    calculator = " ",
    scissor = " ",
    pencile = "↨ ",
    arrow_right = "> ",
    closed = " ",
    x = "x ",
    v = "v ",
    star = "* ",
    circle = "o ",
    debug = "$ ",
    connect = " ",
    bulb = " ",
    tree = " ",
    gear = " ",
}

M.sign = {
    exclamation = "!",
    info = "i",
    warning = "W",
    error = "E",
    bang = "!",
    plus = "+",
    minus = "-",
}

M.file = {
    default = " ",
    symlink = "@",
    folder_closed = "+",
    folder_open = "-",
}

M.git = {
    added = "",
    modified = "",
    unstaged = "M",
    staged = "S",
    unmerged = "O",
    renamed = "R",
    deleted = "D",
    untracked = "?",
    ignored = " ",
    conflict = "%",
    add = "+",
    change = "|",
    change_delete = "~",
    delete = "-",
    delete_bottom = "_",
    delete_top = "‾",
}

M.lsp = {
    Text = "T",
    Method = "ƒ",
    Function = "F",
    Constructor = "F",
    Field = "P",
    Variable = "V",
    Class = "C",
    Interface = "I",
    Module = "C",
    Property = "P",
    Unit = "",
    Value = "V",
    Enum = "E",
    Keyword = "B",
    Snippet = "S",
    Color = "H",
    File = "Z",
    Reference = "R",
    Folder = "Z",
    EnumMember = "E",
    Constant = "V",
    Struct = "C",
    Event = "F",
    Operator = "O",
    TypeParameter = "C",
}

-- Override if has fonts
if require('configs').has_fonts then
    M.general.bar = "▊"
    M.general.scissor = " "
    M.general.calculator = " "
    M.general.pencile = "✎ "
    M.general.arrow_right = "➜ "
    M.general.closed = " "
    M.general.x = "✗ "
    M.general.v = "✓ "
    M.general.star = "★ "
    M.general.circle = " "
    M.general.debug = " "
    M.general.connect = " "
    M.general.bulb = " "
    M.general.tree = " "
    M.general.gear = " "

    M.sign.exclamation = " "
    M.sign.info = " "
    M.sign.warning = " "
    M.sign.error = " "
    M.sign.bang = " "
    M.sign.plus = " "
    M.sign.minus = " "

    M.file.folder_closed = " "
    M.file.folder_open = " "
end

return M
