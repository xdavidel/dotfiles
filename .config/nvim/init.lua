if not vim.fn.has("nvim-0.8") then
    assert(false, "config require nvim-0.8")
end

function P(arg)
  print(vim.inspect(arg))
end

require("settings")
require("mappings")

-- Setup before all the lua plugins
local has_impatient, impatient = pcall(require, 'impatient')
if has_impatient then
    impatient.enable_profile()
end

require("autocmds")
require("plugins")
