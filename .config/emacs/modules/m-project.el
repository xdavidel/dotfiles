;;;; m-project.el --- Starting configuration for project management  -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

;; Provides default settings for project management with project.el

(use-package projectile
  :diminish projectile-mode
  :hook (prog-mode . projectile-mode)
  :general
  (projectile-mode-map
       "C-c p" 'projectile-command-map)
  ('normal :prefix "<leader>" "sp" '("search project"   . projectile-switch-project))
  :config
  (projectile-mode 1)
  (setq projectile-git-submodule-command nil)
  (setq projectile-indexing-method 'alien))

;; [built-in] Project Managment
;; As the built-in project.el support expects to use vc-mode hooks to
;; find the root of projects we need to provide something equivalent
;; for it.
(use-package project
  :straight (:type built-in)
  :config
  (defvar project-root-markers
    '(".git" "CMakeList.txt" "package.clj" "package.json" "mix.exs" "Project.toml" ".project" "Cargo.toml"))

  (defun project-custom-find-root (path)
    (let* ((this-dir (file-name-as-directory (file-truename path)))
           (parent-dir (expand-file-name (concat this-dir "../")))
           (system-root-dir (expand-file-name "/")))
      (cond
       ((project-custom-root-p this-dir) (cons 'transient this-dir))
       ((equal system-root-dir this-dir) nil)
       (t (project-custom-find-root parent-dir)))))

  (defun project-custom-root-p (path)
    (let ((results (mapcar (lambda (marker)
                             (file-exists-p (concat path marker)))
                           project-root-markers)))
      (eval `(or ,@ results))))

  (add-to-list 'project-find-functions #'project-custom-find-root))


(provide 'm-project)
;;; m-project.el ends here
