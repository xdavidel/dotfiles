;;; m-evil.el -- vi-mode -*- lexical-binding: t; -*-

;;; Commentary:

;; Evil mode configuration, for a `vi' keybindings.

;;; Code:

;; Turn on undo-tree globally
(use-package undo-tree
  :diminish
  :init
  (setq undo-tree-history-directory-alist (list (cons "." (concat user-emacs-directory "undo-tree-hist/"))))
  (global-undo-tree-mode))

;; Vim-like keybindings
(use-package evil
  :general
  (evil-insert-state-map
        "C-g" 'evil-normal-state
        "C-h" 'evil-delete-backward-char-and-join)
  (evil-window-map
        "<left>"  'evil-window-left
        "<up>"    'evil-window-up
        "<right>" 'evil-window-right
        "<down>"  'evil-window-down
        "C-g"     'evil-normal-state
        "C-h"     'evil-delete-backward-char-and-join)
  :custom
  (evil-want-integration t)
  (evil-want-keybinding nil)
  (evil-want-C-u-scroll t)
  (evil-want-C-i-jump nil)
  (evil-respect-visual-line-mode t)
  (evil-undo-system 'undo-tree)
  (evil-vsplit-window-right t)
  (evil-split-window-below t)
  :init
  (setq evil-want-Y-yank-to-eol t)
  (evil-mode 1)
  :config
  (evil-set-leader 'normal emacs-leader-key)
  ;; Rebind `universal-argument' to 'C-M-u' since 'C-u' now scrolls the buffer
  (global-set-key (kbd "C-M-u") 'universal-argument)

  ;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  ;; Remove those evil bindings
  (with-eval-after-load 'evil-maps
    (define-key evil-motion-state-map (kbd "SPC") nil)
    (define-key evil-motion-state-map (kbd "RET") nil)
    (define-key evil-motion-state-map (kbd "TAB") nil))

  ;; make evil-search-word look for symbol rather than word boundaries
  (with-eval-after-load 'evil
    (defalias #'forward-evil-word #'forward-evil-symbol)
    (setq-default evil-symbol-word-search t)))

;; Vim like surround package
(use-package evil-surround
  :init
  (global-evil-surround-mode))

(use-package evil-collection
  :diminish evil-collection-unimpaired-mode
  :after evil
  :init
  (evil-collection-init))

;; Terminal cursor mode support
(unless (display-graphic-p)
  (use-package evil-terminal-cursor-changer
    :init
    (evil-terminal-cursor-changer-activate)))

;; Comment code efficiently
(use-package evil-nerd-commenter
  :general
  (global-map
   "C-/" 'evilnc-comment-or-uncomment-lines
   [remap comment-dwim] 'evilnc-comment-or-uncomment-lines)
  :after evil
  :commands (evilnc-comment-or-uncomment-lines)
  :init
  ;; Set a global binding for better line commenting/uncommenting
  (general-def 'normal "gcc" 'evilnc-comment-or-uncomment-lines)
  (general-def 'visual "gc"  'evilnc-comment-or-uncomment-lines))

;; Increment / Decrement binary, octal, decimal and hex literals
(use-package evil-numbers
  :init
  (general-def '(normal visual) "C-a" 'evil-numbers/inc-at-pt)
  (general-def '(normal visual) "g C-a" 'evil-numbers/inc-at-pt-incremental)
  :commands (evil-numbers/inc-at-pt evil-numbers/inc-at-pt-incremental))

;;; Evil Bindings

;; Applications
(evil-define-key 'normal 'global (kbd "<leader>a")  '("application" . (keymap)))

;; Buffers & windows
(evil-define-key 'normal 'global (kbd "<leader>b")  '("buffer"        . (keymap)))
(evil-define-key 'normal 'global (kbd "<leader>bs") '("switch buffer" . switch-to-buffer))
(evil-define-key 'normal 'global (kbd "<leader>bi") '("indent buffer" . indent-whole-buffer))
(evil-define-key 'normal 'global (kbd "<leader>be") '("show diff"     . ediff-buffers))

;; Config
(evil-define-key 'normal 'global (kbd "<leader>c")  '("config"               . (keymap)))
(evil-define-key 'normal 'global (kbd "<leader>cr") '("reload configuration" . reload-configuration))
(evil-define-key 'normal 'global (kbd "<leader>c.") '("open configuration"   . open-config-dir))

;; Files
(evil-define-key 'normal 'global (kbd "<leader>f")  '("file"          . (keymap)))
(evil-define-key 'normal 'global (kbd "<leader>ff") '("find file"     . find-file))

;; Git
(evil-define-key 'normal 'global (kbd "<leader>g")  '("git" . (keymap)))

;; Lsp
(evil-define-key 'normal 'global (kbd "<leader>l")  '("lsp"            . (keymap)))

;; Org
(evil-define-key 'normal 'global (kbd "<leader>o")  '("org"         . (keymap)))

;; Quiting
(evil-define-key 'normal 'global (kbd "<leader>q") '("quit" . (keymap)))
(evil-define-key 'normal 'global (kbd "<leader>qq") '("close buffer" . kill-buffer-and-window))

;; Search
(evil-define-key 'normal 'global (kbd "<leader>s")   '("search"           . (keymap)))
(evil-define-key 'normal 'global (kbd "<leader>si")  '("internet"         . (keymap)))

;; Window
(evil-define-key 'normal 'global (kbd "<leader>w")  '("window"          . (keymap)))
(evil-define-key 'normal 'global (kbd "<leader>ww") '("tear off window" . tear-off-window))
(evil-define-key 'normal 'global (kbd "<leader>wh") '("swap left"       . windmove-swap-states-left))
(evil-define-key 'normal 'global (kbd "<leader>wj") '("swap down"       . windmove-swap-states-down))
(evil-define-key 'normal 'global (kbd "<leader>wk") '("swap up"         . windmove-swap-states-up))
(evil-define-key 'normal 'global (kbd "<leader>wl") '("swap right"      . windmove-swap-states-right))

(general-def 'normal prog-mode-map
  "K" 'eldoc-print-current-symbol-info)

(general-def 'normal emacs-lisp-mode-map
  "K" 'describe-thing-at-point)

(evil-define-key 'normal 'compilation-mode-map
  (kbd "C-n") 'compilation-next-error
  (kbd "C-p") 'compilation-previous-error)

(evil-define-key 'normal 'lsp-mode-map
  (kbd "C-p") 'lsp-find-references)

(provide 'm-evil)
;;; m-evil.el ends here
