;;; m-ui.el -- ui related configuration -*- lexical-binding: t; -*-

;;; Commentary:
;; User interface customizations.
;; Examples are the modeline and how help buffers are displayed.

;;; Code:

;; Emacs does not need pager
(setenv "PAGER" "cat")

(use-package all-the-icons
  :diminish
  :config
  (when (and (not (font-installed-p "all-the-icons"))
             (window-system))
    (all-the-icons-install-fonts t)))

;; Icons for dired
(use-package all-the-icons-dired
  :diminish
  :hook (dired-mode . all-the-icons-dired-mode))

;;;; Theme
(defcustom use-light-theme nil
  "Should Emacs use light theme by default.")

(use-package doom-themes
  :custom
  (doom-themes-enable-bold t)
  (doom-themes-enable-italic t)
  :config
  (doom-themes-visual-bell-config))

(use-package modus-themes
  :general
  (global-map
   "C-c t t" 'modus-themes-toggle)
  :init
  (setq modus-themes-italic-constructs t
        modus-themes-bold-constructs t
        modus-themes-region '(bg-only)
        modus-themes-prompts '(bold intense)
        modus-themes-fringes 'subtle
        modus-themes-tabs-accented t
        modus-themes-paren-match '(bold intense)
        modus-themes-mode-line '(borderless accented)
        modus-themes-org-blocks 'tinted-background
        modus-themes-scale-headings t
        modus-themes-headings
        '((1 . (rainbow overline background 1.4))
          (2 . (rainbow background 1.3))
          (3 . (rainbow bold 1.2))
          (t . (semilight 1.1))))
  ;; Load the theme of your choice
  (if use-light-theme
      (load-theme 'modus-operandi t)
    (load-theme 'modus-vivendi t)))


;; Clear background on changing theme
(unless (display-graphic-p)
  (add-hook 'after-load-theme-hook #'clear-bg))

;; Modeline (status bar)
(use-package doom-modeline
  :hook (after-init . doom-modeline-mode)
  :disabled
  :config
  (when (display-graphic-p)
    (setq doom-modeline-icon t)))

;; Line numbers
(use-package display-line-numbers
  :straight (:type built-in)
  :hook
  (prog-mode . display-line-numbers-mode)
  :custom
  (display-line-numbers-width 4)
  (display-line-numbers-width-start t)
  (display-line-numbers-type 'relative))

(use-package column-number
  :straight (:type built-in)
  :hook
  (prog-mode . column-number-mode))

;; Better help override bindings
(use-package helpful
  :general
  (global-map
   [remap describe-function] 'helpful-callable
   [remap describe-symbol]   'helpful-symbol
   [remap describe-variable] 'helpful-variable
   [remap describe-command]  'helpful-command
   [remap describe-key]      'helpful-key
   "C-h C-k"                 'helpful-at-point
   "C-h K"                   'describe-keymap
   "C-h F"                   'helpful-function)
  (helpful-mode-map
   [remap revert-buffer]     'helpful-update))

;; Set fonts if possible
(cond ((font-installed-p "Cascadia Code")
       (set-face-attribute 'default nil :font "Cascadia Code 10"))
      ((font-installed-p "JetBrainsMono")
       (set-face-attribute 'default nil :font "JetBrainsMono 10"))
      ((font-installed-p "Hack")
       (set-face-attribute 'default nil :font "Hack 10")))


;; add visual pulse when changing focus, like beacon but built-in
(use-package pulse
  :straight (:type built-in)
  :unless (display-graphic-p)
  :init
  (defun pulse-line (&rest _)
    (pulse-momentary-highlight-one-line (point)))
  (dolist (cmd '(recenter-top-bottom
                 other-window windmove-do-window-select
                 pager-page-down pager-page-up
                 winum-select-window-by-number
                 ;; treemacs-select-window
                 symbol-overlay-basic-jump))
    (advice-add cmd :after #'pulse-line))) ; Highlight cursor postion after movement

;; Show eldoc information in popup
(use-package eldoc-box
  :diminish (eldoc-mode eldoc-box-hover-at-point-mode)
  :if (display-graphic-p)
  :config
  :hook (eldoc-mode . eldoc-box-hover-at-point-mode))


;; Context menu for Emacs >= 28
(when (not (version< emacs-version "28"))
  (use-package mouse
    :straight (:type built-in)
    :init
    (context-menu-mode 1)))


(provide 'm-ui)
;;; m-ui.el ends here
