;;; m-shell.el --- Shells configuration -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

;; VTerm
(when IS-LINUX
  (use-package vterm
    :commands (vterm)
    :config
    (setq vterm-shell (getenv "SHELL"))
    :custom
    (vtrem-max-scrollback 10000)
    (vtrem-kiil-buffer-on-exit t)))

;; Eshell
(use-package eshell
  :straight (:type built-in)
  :hook (eshell-first-time-mode . (lambda ()
                                    (evil-define-key '(normal insert visual) 'eshell-mode-map (kbd "C-r") 'consult-history)
                                    (evil-define-key '(normal insert visual) 'eshell-mode-map (kbd "<home>") 'eshell-bol)))
  :init
  (setq eshell-aliases-file (concat user-emacs-directory-orig "/eshell/alias"))
  :config
  (defun curr-dir-path (&optional pwd)
    "Get formatted PWD for shell prompt"
    (interactive "DDirectory: \n")
    (let* ((current-path (if pwd pwd (eshell/pwd))))
      (abbreviate-file-name current-path)))

  (defun curr-dir-git-branch-string (&optional pwd)
    "Returns current git branch as a string, or a space"
    (interactive "DDirectory: \n")
    (let* ((default-directory (if pwd pwd (eshell/pwd)))
           (git-branch (replace-regexp-in-string "\n$" "" (shell-command-to-string "git rev-parse --abbrev-ref HEAD")))
           (has-path (and (not (string-match "^fatal" git-branch))
                          (not (file-remote-p default-directory)))))
      (if (not has-path) " " (concat " (" git-branch ") "))))
  (setq eshell-banner-message ""
        eshell-highlight-prompt nil
        eshell-history-size         10000
        eshell-buffer-maximum-lines 10000
        eshell-highlight-prompt t
        eshell-scroll-to-bottom-on-input t
        eshell-glob-case-insensitive t
        eshell-cmpl-ignore-case t
        eshell-hist-ignoredups t
        eshell-prompt-function
        (lambda ()
          (concat
           (propertize "[" 'face `(:foreground "red" :weight bold))
           (propertize (user-login-name) 'face `(:foreground "yellow" :weight bold))
           (propertize "@" 'face `(:foreground "green" :weight bold))
           (propertize (system-name) 'face `(:foreground "blue" :weight bold))
           (propertize "] " 'face `(:foreground "red" :weight bold))
           (propertize (curr-dir-path) 'face `(:foreground "purple" :weight bold))
           (propertize (curr-dir-git-branch-string) 'face `(:foreground "white"))
           (propertize (format-time-string "%H:%M:%S" (current-time)) 'face `(:foreground "yellow"))
           "\n"
           (propertize (if (= (user-uid) 0) " # " " $ ") 'face `(:foreground "white")))))

  (add-hook 'eshell-mode-hook (lambda () (local-set-key (kbd "C-l") (lambda () (interactive) (eshell/clear-scrollback) (eshell-emit-prompt)))))

  (with-eval-after-load 'esh-opt
    (setq eshell-destroy-buffer-when-process-dies t
          eshell-visual-commands '("htop" "top" "zsh" "bash" "vim" "nvim" "lf" "vifm"))))

;; Provide syntax highlights for eshell
(use-package eshell-syntax-highlighting
  :hook
  (eshell-mode . eshell-syntax-highlighting-mode)
  :config
  ;; Enable in all Eshell buffers.
  (eshell-syntax-highlighting-global-mode +1))

(use-package esh-autosuggest
  :hook eshell) ;company for eshell

(defadvice term-handle-exit
    (after term-kill-buffer-on-exit activate)
  "Kill buffer on term exit."
  (kill-buffer))

(defun uterm ()
  "Open a uniquely named term."
  (interactive)
  (let ((use-shell (cond (IS-WINDOWS "cmd")
                         (IS-LINUX "bash")
                         (t ""))))
    (term use-shell)
    (rename-uniquely)))

(provide 'm-shell)
;;; m-shell.el ends here
