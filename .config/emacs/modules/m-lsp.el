;;; m-lsp.el -- language server protocol -*- lexical-binding: t; -*-

;;; Commentary:

;; Setup lsp packages.

;;; Code:

(use-package flycheck
  :diminish flycheck-mode
  :hook
  (after-init . global-flycheck-mode)
  :config
  (when (featurep 'evil)
    (evil-define-key 'normal 'global "gl" #'flycheck-display-error-at-point)))

;; Show in posframe
(use-package flycheck-posframe
  :after flycheck
  :hook (flycheck-mode . flycheck-posframe-mode))

;; Yaml linter with flycheck
(use-package flycheck-yamllint
  :if (executable-find "yamllint")
  :hook ((yaml-mode . flycheck-yamllint-setup)
         (yaml-mode . flycheck-mode)))

(use-package yasnippet
  :diminish yas-minor-mode
  :hook
  (after-init . yas-global-mode)
  (org-mode . yas-global-mode)
  :commands yas-insert-snippet
  :config
  (yas-reload-all))

;; Another LSP implementation
(use-package eglot
  :commands eglot
  :config
  (add-to-list 'eglot-server-programs
               '(c-mode . ("clangd")))
  (add-to-list 'eglot-server-programs
               '(rust-mode . ("rust-analyzer")))
  (add-to-list 'eglot-server-programs
               '(v-mode . ("vls")))
  (add-to-list 'eglot-server-programs
               '(v-mode . ("vls")))
  (add-to-list 'eglot-server-programs
               '(zig-mode . ("zls")))
  :custom
  ;; Shutdown server when last managed buffer is killed
  (eglot-autoshutdown t))

;; LSP client
(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :general
  ('normal :prefix "<leader>" "lt" '("start server"   . lsp))
  ('normal :prefix "<leader>" "lI" '("install server" . lsp-installer-server))
  ('normal :prefix "<leader>" "ld" '("disconnect"     . lsp-disconnect))
  :init
  (setq lsp-auto-guess-root nil
        lsp-keymap-prefix "C-c l"
        lsp-headerline-breadcrumb-segments '(path-up-to-project file symbols))
  :custom
  (lsp-idle-delay 0.5)
  (lsp-headerline-breadcrumb-mode)
  (lsp-enable-folding nil) ;potential to be slow
  (lsp-enable-text-document-color nil) ;potential to be slow
  (lsp-keep-workspace-alive nil) ; terminate server if last workspace buffer is closed
  (lsp-enable-on-type-formatting nil) ;don't format automatically
  (lsp-completion-provider :none)) ;; using corfu, not company

;; Ui for lsp
(use-package lsp-ui
  :after lsp-mode
  :hook (lsp-mode . lsp-ui-mode)
  :custom
  (lsp-ui-doc-enable t)
  (lsp-ui-doc-show-with-cursor t)
  (lsp-ui-sideline-enable nil)
  (lsp-ui-sideline-show-hover nil)
  (lsp-ui-sideline-show-code-actions nil))

;; Add pyright as a server
(use-package lsp-pyright)

;; Debugger
(use-package dap-mode
  :after lsp-mode
  :straight (dap-mode :includes (dap-python dap-cpptools)
                      :type git
                      :host github
                      :repo "emacs-lsp/dap-mode")
  :general
  (lsp-mode-map
   "<f2>"  'dap-breakpoint-toggle
   "<f5>"  'dap-debug
   "<f6>"  'dap-hydra
   "<f8>"  'dap-continue
   "<f9>"  'dap-next
   "<f11>" 'dap-step-in
   "<f10>" 'dap-step-out)
  :config
  (setq lsp-enable-dap-auto-configure nil
        dap-ui-controls-mode nil
        dap-ui-mode 1
        dap-tooltip-mode 1))

(use-package dap-cpptools
  :after (lsp-mode cc-mode)
  :demand)

(use-package dap-python
  :after dap-mode python
  :demand ; so it loads, "requires", dap-python
  :init
  (setq dap-python-debugger 'debugpy))


(provide 'm-lsp)
;;; m-lsp.el ends here
