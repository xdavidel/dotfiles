;;; m-tools.el -- aditional tools -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

;; More tools to include in emacs
(use-package magit
  :commands (magit magit-status)
  :general
  (global-map
   "C-x g" 'magit-status)
  ('normal :prefix "<leader>" "gs" '("status"    . magit-status))
  ('normal :prefix "<leader>" "gm" '("git blame" . magit-blame-addition))
  :init
  (defun magit-dotfiles()
    (interactive)
    (when IS-LINUX
      (magit-status (expand-file-name "~/.local/dotfiles")))))

(use-package ranger
  :general
  (global-map
   "M-e" 'ranger)
  :config
  (setq ranger-show-literal nil
        ranger-override-dired-mode t)
  :custom
  (ranger-width-preview 0.5))

;; Buitin file manager
(use-package dired
  :straight (:type built-in)
  :commands dired
  :general
  (dired-mode-map
   "<backspace>" 'dired-up-directory
   "C-c C-d"     'mkdir
   "-"           'dired-up-directory)
  ('normal :prefix "<leader>" "e"  '("explorer"   . dired-sidebar-toggle-sidebar))
  ('normal :prefix "<leader>" "fd" '("open dired" . dired-jump))
  :custom ((dired-listing-switches "-aghoA --group-directories-first"))
  :config
  (setq dired-omit-files
        (rx (or (seq bol (? ".") "#")
                (seq bol "." eol)
                (seq bol ".." eol)))))


;; open dired as a sidebar
(use-package dired-sidebar
  :general
  (global-map
   "C-x C-j" 'dired-sidebar-jump)
  :hook (dired-sidebar-mode . visual-line-mode)
  :custom
  (dired-sidebar-use-custom-font t)
  (dired-sidebar-one-instance-p t)      ; just sidebar per frame
  :config
  (defun dired-sidebar-jump ()
    (interactive)
    (dired-sidebar-show-sidebar)
    (dired-sidebar-jump-to-sidebar))

  ;; avoid fixing window size
  (add-hook 'dired-sidebar-mode-hook #'(lambda ()
                                         (setq window-size-fixed nil))))

;; RSS feed
(use-package elfeed
  :commands (elfeed)
  :general
  (elfeed-search-mode-map "C-c u" 'elfeed-update)
  ('normal :prefix "<leader>" "an" '("rss feeds"   . elfeed))
  :custom
  (elfeed-feeds '(
                  ;; dev.to
                  ("http://dev.to/feed" dev)

                  ;; Reddit
                  ("http://reddit.com/r/C_Programming/.rss" dev c)
                  ("http://reddit.com/r/emacs/.rss" text editors)
                  ("http://reddit.com/r/golang/.rss" dev go)
                  ("http://reddit.com/r/rust/.rss" dev rust)

                  ;; Blogs
                  ("https://lukesmith.xyz/rss.xml" blog)

                  ;; Twitter
                  ("https://nitter.net/LiveOverflow/rss" ~LiveOverflow twitter)
                  ;; Tech news

                  ("https://news.ycombinator.com/rss" tech news)
                  ("https://www.archlinux.org/feeds/news" tech news arch)
                  ("https://lwn.net/headlines/rss" tech linux news)

                  )))

(use-package disk-usage
  :commands (disk-usage))


(use-package transmission
  :general
  (transmission-mode-map
   "C-c a" 'transmission-add
   "C-c d" 'transmission-delete
   "C-c t" 'transmission-toggle
   "C-c f" 'transmission-files)
  (global-map "<leader> at" 'transmission)
  :config
  (setq transmission-refresh-modes '(transmission-mode))
  :commands (transmission))

;; Support for pdfs (requires msys on windows)
(use-package pdf-tools
  :mode ("\\.[pP][dD][fF]\\'" . pdf-tools-install)
  :magic ("%PDF" . pdf-tools-install)
  :hook
  (pdf-view-mode . pdf-view-themed-minor-mode)
  :config
  (define-pdf-cache-function pagelabels))

;; open with external program
(use-package openwith
  :defer 1
  :config
  (let ((video-formats (openwith-make-extension-regexp
                        '("mpg" "mpeg" "mp3" "mp4"
                          "avi" "wmv" "wav" "mov" "flv"
                          "ogm" "ogg" "mkv"))))
    (setq openwith-associations
          (list (list video-formats "mpv" '(file))))
    (add-to-list 'recentf-exclude video-formats))
  (openwith-mode))


;; browser the web inside emacs
(use-package eww
  :straight (:type built-in)
  :general
  (global-map
   "<f12>" 'eww)
  :hook (eww-mode . (lambda () (eww-readable)))
  :config
  (setq shr-use-fonts  nil        ; No special fonts
        shr-use-colors t          ; colours
        shr-inhibit-images nil    ; inhibit images
        shr-indentation 2         ; Left-side margin
        shr-width 70              ; Fold text to 70 columns
        shr-color-visible-luminance-min 80
        eww-search-prefix "https://search.brave.com/search?q="))


;; Search engines
(use-package engine-mode
  :straight (:branch "main")
  :general
  ('normal :prefix "<leader>" "sia" '("search archwiki"  . engine/search-archwiki))
  ('normal :prefix "<leader>" "sic" '("search c++"       . engine/search-cppreference))
  ('normal :prefix "<leader>" "sib" '("search cmake"     . engine/search-cmake))
  ('normal :prefix "<leader>" "sid" '("search dockerhub" . engine/search-dockerhub))
  ('normal :prefix "<leader>" "sig" '("search google"    . engine/search-google))
  ('normal :prefix "<leader>" "siG" '("search github"    . engine/search-github))
  ('normal :prefix "<leader>" "sir" '("search rustdoc"   . engine/search-rustdoc))
  ('normal :prefix "<leader>" "siw" '("search wikipedia" . engine/search-wikipedia))
  ('normal :prefix "<leader>" "siy" '("search youtube"   . engine/search-youtube))
  :config
  (defengine archwiki
    "https://wiki.archlinux.org/index.php?title=Special:Search&search=%s")
  (defengine cppreference
    "https://en.cppreference.com/mwiki/index.php?search=%s")
  (defengine cmake
    "https://cmake.org/cmake/help/latest/search.html?q=%s&check_keywords=yes&area=default")
  (defengine google
    "https://google.com/search?q=%s")
  (defengine youtube
    "https://www.youtube.com/results?search_query=%s")
  (defengine dockerhub
    "https://hub.docker.com/search?q=%s&type=image")
  (defengine github
    "https://github.com/search?q=%s")
  (defengine rustdoc
    "https://doc.rust-lang.org/rustdoc/what-is-rustdoc.html?search=%s")
  (defengine wikipedia
    "https://en.wikipedia.org/wiki/%s"))

(use-package popper
  :general
  (global-map "C-`"   'popper-toggle-latest
              "M-`"   'popper-cycle
              "C-M-`" 'popper-toggle-type)
  :init
  (setq popper-reference-buffers
        '("\\*Messages\\*"
          "\\*Warnings\\*"
          "\\*Compile-Log\\*"
          "\\*compilation\\*"
          "\\*Flycheck errors\\*"
          "\\*Org PDF LaText Output\\*"
          "\\*shell\\*"
          "\\*eldoc\\*"
          "\\*helpful .*\\*$"
          "\\*xref\\*"
          "\\*Man [0-9]+.*\\*$"
          "\\*Backtrace\\*"
          "Output\\*$"
          ("^\\*Async.*" . hide)
          help-mode
          compilation-mode
          messages-mode
          occur-mode)
        popper-window-height
        (lambda (win)
          (let* ((height (window-height (frame-root-window) t))
                 (size (floor (* height 0.8))))
            (fit-window-to-buffer win size size))))
  (popper-mode +1)
  (popper-echo-mode +1))

(use-package restart-emacs)


;; Minimizes GC interferecen with user activity
(use-package gcmh
  :diminish gcmh-mode
  :init
  (setq gcmh-idle-delay 5
        gcmh-high-cons-threshold (* 16 1024 1024)) ; 16M
  (gcmh-mode 1))

(use-package diminish
  :demand)


;; Async compilation of packages
(use-package async
  :diminish dired-async-mode
  :init
  (dired-async-mode 1)
  (async-bytecomp-package-mode 1)
  :custom (async-bytecomp-allowed-packages '(all)))


;; Adjusting the calendar
(use-package calendar
  :straight (:type built-in)
  :config
  ;; Remove unneeded holidays
  (setq holiday-oriental-holidays nil
        holiday-bahai-holidays nil
        holiday-islamic-holidays nil
        holiday-solar-holidays nil
        holiday-christian-holidays nil)
  (setq calendar-week-start-day 0))


;;; Lightweight spell check
(use-package spell-fu
  :disabled
  :when (executable-find "aspell")
  :hook ((org-mode git-commit-mode markdown-mode) . spell-fu-mode))

;; 'flyspell' uses `hooks` and `sit-for` to delay
;; this uses `idle-timers`
(use-package flyspell-lazy
  :hook
  (flyspell-mode . flyspell-lazy-mode)
  :config
  (setq flyspell-lazy-idle-seconds 1))

;; convenient functions for correcting
(use-package flyspell-correct
  :general
  (flyspell-mode-map "C-," 'flyspell-correct-wrapper)
  :after flyspell)

;; attempt to make 'flyspell' faster by restricting to region, instead of buffer
(use-package wucuo
  :when (or (executable-find "ispell")
            (executable-find "hunspell"))
  :hook
  (text-mode . wucuo-start)
  (prog-mode . wucuo-start))

(provide 'm-tools)
;;; m-tools.el ends here
