;;; package --- m-org.el  -*- lexical-binding: t; -*-

;;; Commentary:
;; All org related configuration

;;; Code:

(use-package org
  :init
  ;; Return or left-click with mouse follows link
  (setq org-return-follows-link t
        org-mouse-1-follows-link t
        org-descriptive-links t) ;; Display links as the description provided
  :mode (("\\.org$" . org-mode))
  :general
  (org-mode-map
   "C-j" 'org-next-visible-heading
   "C-k" 'org-previous-visible-heading)
  ('normal :prefix "<leader>" "oa" '("org agenda"  . org-agenda))
  ('normal :prefix "<leader>" "oc" '("org capture" . org-capture))
  ('normal :prefix "<leader>" "ot" '("org todos"   . org-todo-list))
  :custom
  (org-hide-emphasis-markers t)
  (org-confirm-babel-evaluate nil)     ; Don't ask before code evaluation
  (org-startup-with-inline-images nil) ; Don't show inline images at startup
  (org-edit-src-content-indentation 0) ; Indentation size for org source blocks
  (org-hide-leading-stars t)           ; One start is fine
  (org-tags-column 0)                  ; tag right after text
  (org-ellipsis " ▼")
  (org-log-done 'time)
  (org-journal-date-format "%B %d, %Y (%A) ")
  (org-journal-file-format "%Y-%m-%d.org")
  (org-directory (let ((dir (file-name-as-directory (expand-file-name "~/Documents/Org"))))
                   (make-directory dir :parents)
                   dir))
  (org-default-notes-file (expand-file-name "notes.org" org-directory))
  (org-capture-templates
   `(("t" "Tasks / Projects")
     ("tt" "Task" entry (file+olp+datetree "tasks.org")
      "* TODO %?\n  %U\n  %a\n  %i" :empty-lines 1)

     ("j" "Journal Entries")
     ("jj" "Journal" entry (file+olp+datetree "journal.org")
      "\n* %<%I:%M %p> - Journal :journal:\n\n%?\n\n"
      :clock-in :clock-resume
      :empty-lines 1)
     ("jm" "Meeting" entry (file+olp+datetree "meetings.org")
      "* %<%I:%M %p> - %a :meetings:\n\n%?\n\n"
      :clock-in :clock-resume
      :empty-lines 1)))
  (org-todo-keywords
   '((sequence
      "TODO(t)"
      "DOING(p)"
      "BUG(b)"
      "WAIT(w)"
      "|"                ; The pipe necessary to separate "active" states and "inactive" states
      "DONE(d)"
      "CANCELLED(c)" )))
  :config
  ;; change header size on different levels
  (dolist (face '((org-level-1 . 1.2)
                  (org-level-2 . 1.1)
                  (org-level-3 . 1.05)
                  (org-level-4 . 1.0)
                  (org-level-5 . 1.1)
                  (org-level-6 . 1.1)
                  (org-level-7 . 1.1)
                  (org-level-8 . 1.1)))
    (set-face-attribute (car face) nil :height (cdr face)))


  (defun org-babel-execute:bat (body params)
    "Execute a block of windows batch script with cmd.exe"
    (let ((shell-file-name "cmd.exe"))
      (org-babel-execute:shell body params)))

  ;; disable auto-pairing of "<" in org-mode
  (add-hook 'org-mode-hook (lambda ()
                             (setq-local electric-pair-inhibit-predicate
                                         `(lambda (c)
                                            (if (char-equal c ?<) t (,electric-pair-inhibit-predicate c))))))

  ;; Org programming languages templates
  (when
      (not
       (version<= org-version "9.1.9"))
    (require 'org-tempo)
    (add-to-list 'org-structure-template-alist
                 '("sh" . "src shell"))
    (add-to-list 'org-structure-template-alist
                 '("el" . "src emacs-lisp"))
    (add-to-list 'org-structure-template-alist
                 '("py" . "src python"))
    (add-to-list 'org-structure-template-alist
                 '("cpp" . "src cpp"))
    (add-to-list 'org-structure-template-alist
                 '("lua" . "src lua"))
    (add-to-list 'org-structure-template-alist
                 '("go" . "src go"))))

;; org export
(use-package ox
  :straight nil
  :after org
  :init
  (setq org-export-in-background t))

(use-package ox-html
  :straight nil
  :after ox
  :config
  ;; don't scale svg images
  (setq org-html-head "<style> .org-svg {width: auto} </style>"))

;; for exporting to markdown
(use-package ox-md
  :straight nil
  :after org)

(use-package ox-latex
  :straight nil
  :if IS-LINUX
  :after ox
  :init
  ;; change scale of latex preview in org-mode
  (setq org-format-latex-options (plist-put org-format-latex-options :scale 1.1)
        ;; org-startup-with-latex-preview t
        org-latex-image-default-width nil ; don't scale my images!
        org-latex-images-centered nil)    ; sometimes I want side-by-side images

  ;; minted code pdf export org
  (setq org-latex-listings 'minted
        org-latex-pdf-process
        '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
          "bibtex %b"
          "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
          "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))

  ;; extra latex packages for every header
  (setq org-latex-packages-alist '(("newfloat" "minted" nil)
                                   ("a4paper, margin=20mm" "geometry" nil)))

  (add-to-list 'org-latex-default-packages-alist '("colorlinks=true, linkcolor=blue, citecolor=blue, filecolor=magenta, urlcolor=cyan" "hyperref" nil)))


(use-package org-clock
  :straight nil
  :after org
  :config
  ;; Save the running clock and all clock history when exiting Emacs, load it on startup
  (setq org-clock-persistence-insinuate t
        org-clock-persist t
        org-clock-in-resume t
        org-clock-out-remove-zero-time-clocks t
        org-clock-mode-line-total 'current
        org-duration-format (quote h:mm)))

(use-package org-src
  :straight nil
  :after org
  :init
  ;; babel and source blocks
  (setq org-src-fontify-natively t
        org-src-window-setup 'current-window ; don't move my windows around!
        org-src-preserve-indentation t       ; preserve indentation in code
        org-adapt-indentation nil            ; no extra space... better use indent mode (virtual)
        org-edit-src-content-indentation 0   ; dont indent source code
        org-src-tab-acts-natively t          ; if t, it is slow!
        org-confirm-babel-evaluate nil))     ; doesn't ask for confirmation

;; Avoid `org-babel-do-load-languages' since it does an eager require.

;; load ob-python only when executing python block
(use-package ob-python
  :straight nil
  :after org
  :commands org-babel-execute:python
  :init
  (setq org-babel-default-header-args:python
        '((:results . "output")
          (:noweb . "no-export") ; referencing other blocks with <<>> syntax, don't expand during export
          (:eval . "never-export") ; don't eval blocks when exporting, except when `:eval yes`
          (:exports . "results")))) ; export only plots by default

(use-package ob-shell
  :straight nil
  :commands
  (org-babel-execute:sh
   org-babel-expand-body:sh

   org-babel-execute:bash
   org-babel-expand-body:bash))

;; load ob-C when executing C++ source block
(use-package ob-C
  :straight nil
  :commands org-babel-execute:C++
  :config
  (setq org-babel-default-header-args:C++
        '((:results . "output")
          (:noweb . "no-export") ; referencing other blocks with <<>> syntax, don't expand during export
          (:eval . "never-export") ; don't eval blocks when exporting, except when `:eval yes`
          (:exports . "results"))))


(use-package ob-js
  :straight nil
  :commands org-babel-execute:js
  :config
  (setq org-babel-default-header-args:js
        '((:results . "output")
          (:noweb . "no-export") ; referencing other blocks with <<>> syntax, don't expand during export
          (:eval . "never-export") ; don't eval blocks when exporting, except when `:eval yes`
          (:exports . "results"))))

;; for UML diagrams in org-mode
;; need to install `plantuml'
(use-package ob-plantuml
  :straight nil
  :commands org-babel-execute:plantuml
  :config
  (setq org-plantuml-exec-mode 'plantuml))

(use-package ob-core
  :straight nil
  :after org
  :init
  ;; mkdirp allows creating the :dir if it does not exist
  (add-to-list 'org-babel-default-header-args '(:mkdirp . "yes"))
  (add-to-list 'org-babel-default-header-args '(:noweb . "no-export")))

;; Show hidden chars in org mode
(use-package org-appear
  :hook (org-mode . org-appear-mode))

;; Table of contents using `:toc:` on a heading
(use-package toc-org
  :diminish
  :hook
  (org-mode . toc-org-mode)
  (markdown-mode . toc-org-mode))

;; Show presentation for org mode
(use-package org-present
  :general (org-present-mode-keymap
            "C-c C-j" 'org-present-next
            "C-c C-k" 'org-present-prev)
  :commands (org-present))

;; eye candy for org
(use-package org-modern
  :when (display-graphic-p)
  :hook (org-mode . org-modern-mode)
  :disabled
  :custom
  (org-modern-star nil))

;; Auto tangle org files only if file has '#+auto_tangle: t'
(use-package org-auto-tangle
  :hook (org-mode . org-auto-tangle-mode))


;; Keep text indented with headlines
(use-package org-indent
  :straight (:type built-in)
  :hook (org-mode . org-indent-mode))

(use-package org-roam
  :init
  (setq org-roam-v2-ack t) ; don't show prompt of the new version
  :custom
  (org-roam-directory (let ((dir (file-name-as-directory (expand-file-name "~/Documents/Org/RoamNotes"))))
                        (make-directory dir :parents)
                        dir))
  (org-roam-completion-everywhere t)
  :hook
  (org-agenda-mode . (lambda() (require 'org-roam)))
  :general
  (global-map
   "C-c n l" 'org-roam-buffer-toggle
   "C-c n f" 'org-roam-node-find
   "C-c n i" 'org-roam-node-insert
   "C-c n I" 'org-roam-node-insert-immediate
   "C-c n p" 'org-roam-find-project
   "C-c n t" 'org-roam-capture-task
   "C-c n d" 'org-roam-jump-menu/body)
  :config
  ;; Bind this to C-c n I
  (defun org-roam-node-insert-immediate (arg &rest args)
    (interactive "P")
    (let ((args (cons arg args))
          (org-roam-capture-templates (list (append (car org-roam-capture-templates)
                                                    '(:immediate-finish t)))))
      (apply #'org-roam-node-insert args)))

  (defun org-roam-filter-by-tag (tag-name)
    (lambda (node)
      (member tag-name (org-roam-node-tags node))))

  (defun org-roam-list-notes-by-tag (tag-name)
    (mapcar #'org-roam-node-file
            (seq-filter
             (org-roam-filter-by-tag tag-name)
             (org-roam-node-list))))

  (defun org-roam-refresh-agenda-list ()
    (interactive)
    (setq org-agenda-files (org-roam-list-notes-by-tag "Project")))


  (defun org-roam-project-finalize-hook()
    "Add the captured project file to `org-agenda-files' if
the capture was not aborted"
    (remove-hook 'org-capture-after-finalize-hook #'org-roam-project-finalize-hook)
    (unless org-note-abort
      (with-current-buffer (org-capture-get :buffer)
        (add-to-list 'org-agenda-files (buffer-file-name)))))

  (defun org-roam-find-project ()
    (interactive)
    (add-hook 'org-capture-finalize-hook #'org-roam-project-finalize-hook)
    ;; Select a project file to open, creating it if necessary
    (org-roam-node-find
     nil
     nil
     (org-roam-filter-by-tag "Project")
     :templates
     '(("p" "project" plain "* Goals\n\n%?\n\n* Tasks\n\n** TODO Add initial tasks\n\n* Dates\n\n"
        :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+category: ${title}\n#+filetags: Project")
        :unnarrowed t))))

  (defun org-roam-capture-task ()
    (interactive)
    ;; Add the project file to the agenda after capture is finished
    (add-hook 'org-capture-after-finalize-hook #'org-roam-project-finalize-hook)

    ;; Capture the new task, creating the project file if necessary
    (org-roam-capture- :node (org-roam-node-read
                              nil
                              (org-roam-filter-by-tag "Project"))
                       :templates '(("p" "project" plain "** TODO %?"
                                     :if-new (file+head+olp "%<%Y%m%d%H%M%S>-${slug}.org"
                                                            "#+title: ${title}\n#+category: ${title}\n#+filetags: Project"
                                                            ("Tasks"))))))

  ;; (setq daily-note-filename-format "%<%Y-%m-%d(%A)>.org"
  ;;       daily-note-header-format "#+title: %<%Y-%m-%d %a>\n\n[[roam:%<%Y-%B>]]\n\n")

  (setq org-roam-dailies-capture-templates (let ((daily-note-filename-format "%<%Y-%m-%d(%A)>.org")
                                                 (daily-note-header-format "#+title: %<%Y-%m-%d %a>\n\n[[roam:%<%Y-%B>]]\n\n"))
                                             `(("d" "default" entry
                                                "* %?"
                                                :if-new (file+head ,daily-note-filename-format
                                                                   ,daily-note-header-format))
                                               ("t" "task" entry
                                                "* TODO %?\n  %U\n  %a\n  %i"
                                                :if-new (file+head+olp ,daily-note-filename-format
                                                                       ,daily-note-header-format
                                                                       ("Tasks"))
                                                :empty-lines 1)
                                               ("l" "log entry" entry
                                                "* %<%I:%M %p> - %?"
                                                :if-new (file+head+olp ,daily-note-filename-format
                                                                       ,daily-note-header-format
                                                                       ("Log")))
                                               ("j" "journal" entry
                                                "* %<%I:%M %p> - Journal  :journal:\n\n%?\n\n"
                                                :if-new (file+head+olp ,daily-note-filename-format
                                                                       ,daily-note-header-format
                                                                       ("Log")))
                                               ("m" "meeting" entry
                                                "* %<%I:%M %p> - %^{Meeting Title}  :meetings:\n\n%?\n\n"
                                                :if-new (file+head+olp ,daily-note-filename-format
                                                                       ,daily-note-header-format
                                                                       ("Log"))))))


  (defhydra org-roam-jump-menu (:hint nil)
    "
^Dailies^        ^Capture^
^^^^^^^^-------------------------
_t_: today       _T_: today
_r_: tomorrow    _R_: tomorrow
_y_: yesterday   _Y_: yesterday
_d_: date        ^ ^
"   ("t" org-roam-dailies-goto-today)
    ("r" org-roam-dailies-goto-tomorrow)
    ("y" org-roam-dailies-goto-yesterday)
    ("d" org-roam-dailies-goto-date)
    ("T" org-roam-dailies-capture-today)
    ("R" org-roam-dailies-capture-tomorrow)
    ("Y" org-roam-dailies-capture-yesterday)
    ("c" nil "cancel"))
  (org-roam-db-autosync-mode)

  ;; Build the agenda list the first time for the session
  (org-roam-refresh-agenda-list))

(provide 'm-org)
;;; m-org.el ends here
