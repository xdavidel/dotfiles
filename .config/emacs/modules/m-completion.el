;;; m-completion.el -- enhance completion system -*- lexical-binding: t; -*-

;;; Commentary:
;; Setup completion packages.
;; Completion in this sense is more like
;; narrowing, allowing the user to find matches based on minimal
;; inputs and "complete" the commands, variables, etc from the
;; narrowed list of possible choices.

;;; Code:

(if (not (version< emacs-version "27"))
    (progn
      (use-package vertico
        :init
        (vertico-mode 1)
        :general
        (vertico-map "C-j"   'vertico-next
                     "C-k"   'vertico-previous
                     "M-h"   'vertico-directory-up
                     "?"     'minibuffer-completion-help
                     "M-RET" 'minibuffer-force-complete-and-exit)
        :custom
        (vertico-cycle t)
        :custom-face
        (vertico-current ((t (:background "#3a3f5a")))))

      (use-package marginalia
        :init
        (marginalia-mode 1)
        :custom
        (marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil)))

      (use-package consult
        :general
        (global-map
         "C-c o" 'consult-imenu     ; navigation by "imenu" items
         "C-c r" 'consult-ripgrep   ; search file contents
         "C-S-f" 'consult-ripgrep   ; search file contents
         "C-x b" 'consult-buffer    ; enhanced switch to buffer
         "M-o"   'consult-outline   ; navigation by headings
         "C-s"   'consult-line      ; search lines with preview
         "M-y"   'consult-yank-pop) ; editing cycle through kill-ring
        (minibuffer-local-completion-map
         "<tab>"  'minibuffer-force-complete)
        ('normal :prefix "<leader>" "fb" '("switch buffer"    . consult-buffer))
        ('normal :prefix "<leader>" "fr" '("recent files"     . consult-recent-file))
        ('normal :prefix "<leader>" "sc" '("change theme"     . consult-theme))
        ('normal :prefix "<leader>" "sR" '("search registers" . consult-register))
        ('normal :prefix "<leader>" "st" '("search text"      . consult-ripgrep))
        :hook (completion-setup . hl-line-mode)
        :custom
        (consult-preview-raw-size 1) ;; Simple preview - no hooks
        (completion-in-region-function #'consult-completion-in-region)
        :config
        ;; configure preview behavior
        (consult-customize
         consult-buffer consult-bookmark :preview-key '(:debounce 0.5 any)
         consult-theme :preview-key '(:debounce 1 any)
         consult-line :preview-key '(:debounce 0 any))

        ;; use 'fd' instead of 'find' if exists in system
        (when (executable-find "fd")
          (setq consult-find-args "fd --hidden"))

      (setq consult-project-root-function
            (lambda ()
              (when-let (project (project-current))
                (car (project-roots project))))))


      ;; Set up Orderless for better fuzzy matching
      (use-package orderless
        :init
        (setq completion-category-defaults nil
              read-file-name-completion-ignore-case t)
        :custom
        (completion-styles '(orderless))
        (completion-category-overrides '((file (styles . (partial-completion)))
                                         (minibuffer (initials)))))

      ;; Better greping
      (use-package affe
        :when IS-LINUX
        :general
        (global-map
         "C-S-f"  'affe-grep
         "C-c f"  'affe-find)
        ('normal :prefix "<leader>" "st" '("search text"      . affe-grep))
        :config
        (defun affe-orderless-regexp-compiler (input _type _ignorecase)
          (setq input (orderless-pattern-compiler input))
          (cons input (lambda (str) (orderless--highlight input str))))
        (setq affe-regexp-compiler #'affe-orderless-regexp-compiler
              affe-count 100)
        (when (executable-find "rg")
          (setq affe-find-command "rg --color=never --files --max-depth 1 -j2"
                affe-grep-command "rg --null --color=never --max-columns=1000 --no-heading --max-depth 1 -j2 --line-number -v ^$ .")))


      ;; completion any text based on buffer contents
      (use-package dabbrev
        :general
        (global-map
         "M-/"   'dabbrev-completion ; this can be completed with corfu
         "C-M-/" 'dabbrev-expand)
        :config
        ;; don't change case
        (setq dabbrev-case-replace nil))

      ;; Completion framwork for anything
      (use-package company
        :diminish
        :general
        (global-map "C-SPC" 'company-complete)
        (company-active-map
              "<down>"    'company-select-next
              "<up>"      'company-select-previous
              "TAB"       'company-complete-selection
              "<tab>"     'company-complete-selection
              "<escape>"  'company-abort
              "RET"       'company-complete-selection
              "<ret>"     'company-complete-selection)
        :hook
        (prog-mode . company-mode)
        (org-mode  . company-mode)
        (text-mode . company-mode)
        :custom
        (company-idle-delay 1)
        (company-minimum-prefix-length 1)
        (company-tooltip-align-annotations t)
        (company-dabbrev-downcase nil)
        (company-selection-wrap-around t)
        (company-format-margin-function #'company-vscode-dark-icons-margin))

      ;; Better sotring company results
      (use-package company-prescient
        :hook
        (company-mode . company-prescient-mode))

      (use-package company-emoji
        :hook
        (company-mode . (lambda() (company-emoji-init))))


      (use-package corfu
        :disabled
        :general
        (global-map
         "C-SPC"    'completion-at-point)
        (corfu-map
              "<tab>"    'corfu-complete
              "C-n"      'corfu-next
              "C-p"      'corfu-previous
              "<escape>" 'corfu-quit)
        :custom
        (corfu-cycle t)                  ;; Enable cycling for `corfu-next/previous'
        (corfu-auto t)                   ;; Enable auto completion
        (corfu-commit-predicate nil)     ;; Do not commit selected candidates on next input
        (corfu-quit-at-boundary t)       ;; Automatically quit at word boundary
        (corfu-quit-no-match t)          ;; Automatically quit if there is no match
        (corfu-preview-current nil)      ;; Disable current candidate preview
        (corfu-preselect-first t)        ;; candidate preselection
        (corfu-echo-documentation nil)   ;; Show documentation in the echo area
        (corfu-scroll-margin 5)          ;; Use scroll margin
        (corfu-count 9)                  ;; Maximal number of candidates to show.
        :init
        (setq corfu-auto-delay 0.4
              corfu-auto-prefix 1)
        (global-corfu-mode)
        (with-eval-after-load 'eshell
          (add-hook 'eshell-mode-hook #'(lambda() (set-local corfu-auto nil)))))

      (use-package svg-lib)

      ;; Add icons into corfu
      (use-package kind-icon
        :straight (kind-icon :type git :host github :repo "jdtsmith/kind-icon") ;; currently required
        :after corfu
        :demand
        :custom
        (kind-icon-default-face 'corfu-default)
        :config
        (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

      ;; Corfu-doc
      (use-package corfu-doc
        :straight (corfu-doc :type git :host github :repo "galeo/corfu-doc")
        :hook
        (corfu-mode . corfu-doc-mode)
        :custom
        (corfu-doc-delay 0.5)
        (corfu-doc-max-width 70)
        (corfu-doc-max-height 20))

      ;; `completion-at-point' extensions for specific candidates in `completion in region`
      (use-package cape
        :init
        (setq dabbrev-upcase-means-case-search t)
        ;; Add `completion-at-point-functions', used by `completion-at-point'.
        (add-to-list 'completion-at-point-functions #'cape-file)     ;; Complete file name
        (add-to-list 'completion-at-point-functions #'cape-dabbrev)  ;; Complete word from current buffers
        (add-to-list 'completion-at-point-functions #'cape-keyword)  ;; Complete programming language keyword
        (add-to-list 'completion-at-point-functions #'cape-tex)      ;; Complete unicode char from TeX command
        (add-to-list 'completion-at-point-functions #'cape-sgml)     ;; Complete unicode char from Sgml entity
        (add-to-list 'completion-at-point-functions #'cape-rfc1345)) ;; Complete unicode char using RFC 1345 mnemonics

      (use-package embark
        :general
        (global-map
              [remap describe-bindings] 'embark-bindings
              "C-." 'embark-act)
        :config
        ;; actions with "@" when in the prompter
        (setq embark-action-indicator
              (lambda (map _target)
                (which-key--show-keymap "Embark" map nil nil 'no-paging)
                #'which-key--hide-popup-ignore-command)
              embark-become-indicator embark-action-indicator
              prefix-help-command #'embark-prefix-help-command))

      (use-package embark-consult
        :demand ;necessary for consult preview
        :hook (embark-collect-mode . consult-preview-at-point-mode)
        :after (embark consult)))

  (progn
    ;; Light narrowing framework
    (use-package ivy
      :commands ivy-mode
      :general
      (ivy-minibuffer-map
                  "C-j" 'ivy-next-line
                  "C-k" 'ivy-previous-line)
      :init
      (ivy-mode 1)
      :custom-face
      (ivy-org ((t (:inherit default))))
      :custom
      (ivy-ignore-buffers '("\\` " "\\`\\*"))
      (ivy-use-selectable-prompt t)
      (ivy-count-format "(%d/%d) ")
      (ivy-display-style 'fancy)
      (ivy-dynamic-exhibit-delay-ms 200)
      (ivy-initial-inputs-alist nil)
      (ivy-re-builders-alist '((t . ivy--regex-ignore-order)))
      (ivy-use-virtual-buffers t)
      (ivy-extra-directories nil))


    ;; Smart sorting and filtering for ivy
    (use-package ivy-prescient
      :after ivy
      :init
      (ivy-prescient-mode 1))

    ;; A better ivy with decriptions
    (use-package ivy-rich
      :after counsel
      :init
      (ivy-rich-mode 1))

    ;; Enhanced configurations of `ivy'
    (use-package counsel
      :after ivy
      :commands (counsel-M-x
                 counsel-find-file
                 counsel-file-jump
                 counsel-recentf
                 counsel-rg
                 counsel-describe-function
                 counsel-describe-variable
                 counsel-find-library)
      :init
      (counsel-mode 1)
      :config
      (use-package smex)
      (when (executable-find "fd")
        (define-advice counsel-file-jump (:around (foo &optional initial-input initial-directory) aorst:counsel-fd)
          (let ((find-program "fd")
                (counsel-file-jump-args (split-string "-L --type f --hidden")))
            (funcall foo initial-input initial-directory))))
      (when (executable-find "rg")
        (setq counsel-rg-base-command
              "rg -S --no-heading --hidden --line-number --color never %s .")))))

(provide 'm-completion)
;;; m-completion.el ends here
