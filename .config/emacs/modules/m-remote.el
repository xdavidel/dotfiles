;;;; m-remote.el --- Configuration for remote development  -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

;; Manage remotes
(use-package tramp
  :straight (:type built-in)
  :config
  ;; Set default connection mode to SSH
  (setq tramp-default-method "ssh"
        tramp-verbose 6))

;; Kubernetes
(use-package kubel
  :general
  ('normal :prefix "<leader>" "ak" '("kubernetes"  . kubel))
  :commands kubel)

;; Docker
(use-package docker
  :commands docker
  :general
  (global-map "C-c d" 'hydra-docker/body)
  ('normal :prefix "<leader>" "ad" '("docker"      . docker))
  :config
  (defhydra hydra-docker (:columns 5 :color blue)
    "Docker"
    ("c" docker-containers "Containers")
    ("v" docker-volumes "Volumes")
    ("i" docker-images "Images")
    ("n" docker-networks "Networks")
    ("b" dockerfile-build-buffer "Build Buffer")
    ("q" nil "Quit")))

(use-package kubernetes
  :general
  (global-map "C-c k" 'hydra-kube/body)
  :commands (kubernetes-overview)
  :config
  (defhydra hydra-kube (:columns 5 :color blue)
    "Kubernetes"
    ("o" kubernetes-overview "Overview")
    ("c" kubernetes-config-popup "Config")
    ("e" kubernetes-exec-popup "Exec")
    ("l" kubernetes-logs-popup "Logs")
    ("L" kubernetes-labels-popup "Labels")
    ("d" kubernetes-describe-popup "Describe")))
(use-package docker-tramp
  :after docker)

(use-package kubernetes-evil
  :after kubernetes)

(provide 'm-remote)
;;; m-remote.el ends here
