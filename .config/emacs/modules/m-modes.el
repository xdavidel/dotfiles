;;; m-modes.el -- adding more modes to Emacs -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

;; Add more known modes into emacs

(use-package prog-mode
  :straight (:type built-in)
  :hook
  (prog-mode . hl-line-mode)
  (emacs-lisp-mode . (lambda () (add-to-list 'write-file-functions (lambda()
                                                                     (when (equal (file-name-extension buffer-file-name) "el")
                                                                       (check-parens)))))))

;; Python
(use-package pyvenv
  :mode ("\\.py\\'" . python-mode)
  :config
  (setq pyvenv-workon "emacs")  ; Default venv
  (pyvenv-tracking-mode 1))  ; Automatically use pyvenv-workon via dir-locals

;; C / C++
(use-package cc-mode
  :straight (:type built-in)
  :mode
  (("\\.c\\'" . c-mode)
   ("\\.cpp\\'" . c-mode)
   ("\\.h\\'" . c-mode)
   ("\\.hpp\\'" . c-mode))
  :hook ((c-mode-common . cc-mode-setup))
  :config
  (defun cc-mode-setup ()
    (c-set-offset 'case-label '+)
    (setq c-basic-offset 4
          c-default-style "linux"
          comment-start "/*"
          comment-end "*/"
          tab-width 4)))

;; Format C code with Clang Format
(use-package clang-format
  :if (executable-find "clang")
  :after cc-mode
  :bind (:map c-mode-base-map
              ("C-c C-M-f" . clang-format-buffer)))

;; Rust syntax highlighting
(use-package rust-mode
  :mode ("\\.rs\\'" . rust-mode)
  :commands (rust-format-buffer)
  :bind (:map rust-mode-map
              ("C-c C-M-f" . rust-format-buffer)))

;; Rust - Cargo integration
(use-package cargo
  :if (executable-find "cargo")
  :hook ((rust-mode toml-mode) . cargo-minor-mode))

(use-package go-mode
  :mode ("\\.go\\'" . go-mode))

;; Syntax highlighting of TOML files
(use-package toml-mode
  :mode ("\\.toml\\'" . toml-mode))

;; Syntax highlighting for zig
(use-package zig-mode
  :mode ("\\.zig\\'" . zig-mode))

(use-package rego-mode
  :mode ("\\.rego\\'"))

;; Syntax highlighting for vimscript
(use-package vimrc-mode
  :mode ("\\.vim\\(rc\\)?\\'" . vimrc-mode))

;; Lisp and ELisp mode
(use-package elisp-mode
  :straight (:type built-in)
  :hook (emacs-lisp-mode . eldoc-mode))

;; Yaml support
(use-package yaml-mode
  :mode ("\\.yaml\\'" . yaml-mode)
  :custom (yaml-indent-offset 4))

(use-package markdown-mode
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'"       . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :config
  (dolist (face '((markdown-header-face-1 . 1.4)
                  (markdown-header-face-2 . 1.3)
                  (markdown-header-face-3 . 1.2)
                  (markdown-header-face-4 . 1.1)
                  (markdown-header-face-5 . 1.0)))
    (set-face-attribute (car face) nil :weight 'normal :height (cdr face)))
  :custom
  (markdown-fontify-code-blocks-natively t)
  (markdown-command "pandoc")
  (markdown-hr-display-char nil)
  (markdown-list-item-bullets '("-")))

;; Javascript
(use-package js2-mode
  :mode "\\.js\\'")

;; Shell scripting
(use-package sh-script
  :straight (:type built-in))

;; Lua mode
(use-package lua-mode
  :mode ("\\.lua\\'" . lua-mode)
  :custom
  (lua-indent-level 2)
  (defvar org-babel-default-header-args:lua '((:results . "silent")))
  (defun org-babel-execute:lua (body _)
    "Evaluate a block of Lua code with Babel."
    (lua-get-create-process)
    (lua-send-string body)))

;; V mode
(use-package v-mode
  :config
  :bind-keymap
  :bind
  (:map v-mode-map
        ("C-c C-f" . v-format-buffer))
  :mode ("\\(\\.v?v\\|\\.vsh\\)$" . 'v-mode))

;; CSS mode
(use-package css-mode
  :straight (:type built-in)
  :mode ("\\.css\\'" . css-mode)
  :custom
  (css-indent-offset 2))

;; JSON mode
(use-package json-mode
  :mode ("\\.json\\'" . json-mode)
  :hook (json-mode . flycheck-mode)
  :custom
  (js-indent-level 2))

(use-package csv-mode
  :custom
  (csv-align-max-width 80))

;; Plantuml mode
(use-package plantuml-mode
  :mode (("\\.plantuml\\'" . plantuml-mode)
         ("\\.pu\\'" . plantuml-mode)))

(use-package ahk-mode
  :mode ("\\.ahk\\'" . ahk-mode))

(use-package powershell
  :mode ("\\.ps1\\'" . powershell-mode))

(use-package hcl-mode
  :mode "\\.nomad\\'")

(use-package dockerfile-mode
  :mode "\\Dockerfile\\'")

(use-package typescript-mode)

(use-package nix-mode
  :mode "\\.nix\\'")

(provide 'm-modes)
;;; m-modes.el ends here
