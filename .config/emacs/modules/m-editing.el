;;; m-editing.el -- editing related stuff -*- lexical-binding: t; -*-

;;; Commentary:

;; Editing text configuration.

;;; Code:

;; Be smart when using parens, and highlight content
(if (fboundp 'electric-pair-mode)
      (electric-pair-mode 1) ; auto-insert matching bracket
  (use-package smartparens
    :hook (prog-mode . smartparens-mode)
    :config
    ;; highligh matching brackets
    (show-smartparens-global-mode 0)
    ;; so that paren highlights do not override region marking (aka selecting)
    (setq show-paren-priority -1)
    (setq show-paren-when-point-inside-paren t)
    (setq sp-show-pair-from-inside t)
    (setq show-paren-style 'mixed)))

;; Show matching parens
(use-package paren
  :straight (:type built-in)
  :init
  (setq show-paren-delay 0)
  (show-paren-mode t))

;; whitespace
(use-package whitespace
  :diminish whitespace-mode
  :straight (:type built-in)
  :custom
  (whitespace-style '(face tabs empty trailing tab-mark indentation::space))
  (whitespace-action '(cleanup auto-cleanup))
  :hook
  (prog-mode . whitespace-mode)
  (text-mode . whitespace-mode))

;; Highlight modified region
(use-package goggles
  :diminish goggles-mode
  :custom
  (goggles-pulse t)
  :hook
  (text-mode . goggles-mode)
  (prog-mode . goggles-mode))

;; Move lines around easily
(use-package drag-stuff
  :diminish drag-stuff-mode
  :init
  (drag-stuff-global-mode 1)
  :general
  (drag-stuff-mode-map
        "M-<up>"   'drag-stuff-up
        "M-<down>" 'drag-stuff-down))

;; Finding Files and URLs at Point
(use-package ffap
  :general ("C-x f" 'ffap)
  :init
  (setq find-file-visit-truename t))

;; Lightweight syntax highlighting improvement for numbers
(use-package highlight-numbers
  :hook (prog-mode . highlight-numbers-mode))

;; Lightweight syntax highlighting improvement for escape sequences (e.g. \n, \t).
(use-package highlight-escape-sequences
  :hook (prog-mode . hes-mode))

;; Set color backgrounds to color names
(use-package rainbow-mode
  :diminish rainbow-mode
  :hook (prog-mode . rainbow-mode))

;; Folds
(use-package hideshow
  :diminish hs-minor-mode
  :straight (:type built-in)
  :hook (prog-mode . hs-minor-mode))

;; Rainbow colors for brackets
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;; Format collection for every mode
(use-package format-all)

;; Move easily in shown region
(use-package avy
  :general
  ("M-s" 'avy-goto-char-timer)
  ('(normal visual) :prefix "<leader>"
   "SPC SPC" '("goto line"      . avy-goto-line)
   "SPC j"   '("jump line down" . avy-goto-line-below)
   "SPC k"   '("jump line up"   . avy-goto-line-above)))

(provide 'm-editing)
;;; m-editing.el ends here
