;;; m-defaults.el -- better defaults -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

;; Revert Dired and other buffers
(customize-set-variable 'global-auto-revert-non-file-buffers t)

;; Revert buffers when the underlying file has changed
(global-auto-revert-mode 1)

;; No line wrap be default
(setq-default truncate-lines t)

;; Use spaces instead of tabs
(setq-default indent-tabs-mode nil)

;; Tabs is this much spaces
(setq-default tab-width 4)

;; No Lock Files
(customize-set-variable 'create-lockfiles nil)

;; slow down update ui
(customize-set-variable 'idle-update-delay 1.0)

;; Don't blink the cursor
(blink-cursor-mode -1)

;; Increase autosave times
(setq auto-save-interval 2400
      auto-save-timeout 300)

;; create directory for auto-save-mode
(let* ((auto-save-path (concat user-emacs-directory "/auto-saves/"))
       (auto-save-sessions-path (concat auto-save-path "sessions/")))
  (make-directory auto-save-path t)
  (setq auto-save-list-file-prefix auto-save-sessions-path
        auto-save-file-name-transforms `((".*" ,auto-save-path t))))

;; Put backups elsewhere
(setq backup-directory-alist
      `(("." . ,(concat user-emacs-directory "backup")))
      backup-by-copying t     ; Use copies
      version-control t       ; Use version numbers on backups
      delete-old-versions t   ; Automatically delete excess backups
      kept-new-versions 10    ; Newest versions to keep
      kept-old-versions 5     ; Old versions to keep
      backup-enable-predicate
      (lambda (name)
        (and (normal-backup-enable-predicate name)
             (not
              (let ((method (file-remote-p name 'method)))
                (when (stringp method)
                  (member method '("su" "sudo" "doas"))))))))


;; N.B. Emacs 28 has a variable for using short answers, which should
;; be preferred if using that version or higher.
(if (boundp 'use-short-answers)
    (setq use-short-answers t)
  (fset 'yes-or-no-p 'y-or-n-p))

;; Windows doesn't always has HOME env.
(when (and IS-WINDOWS (null (getenv-internal "HOME")))
  (setenv "HOME" (getenv "USERPROFILE"))
  (setq abbreviated-home-dir nil))

;; Make UTF-8 the default coding system
(set-language-environment "UTF-8")
(unless IS-WINDOWS
  (setq selection-coding-system 'utf-8))

;; Don't render cursors or regions in non-focused windows.
(setq-default cursor-in-non-selected-windows nil)
(setq highlight-nonselected-windows nil)

;; Rapid scrolling over unfontified regions.
(setq fast-but-imprecise-scrolling t)

;; Font compacting can be terribly expensive.
(setq inhibit-compacting-font-caches t
      redisplay-skip-fontification-on-input t)

;; maybe improve performance on windows
(when IS-WINDOWS
  (setq w32-get-true-file-attributes nil   ; decrease file IO workload
        w32-pipe-read-delay 0              ; faster IPC
        w32-pipe-buffer-size (* 64 1024))) ; read more at a time (was 4K)

;; Remove cmdlines options that aren't relevant to our current OS.
(unless IS-MAC   (setq command-line-ns-option-alist nil))
(unless IS-LINUX (setq command-line-x-option-alist nil))

;; Recent files
(use-package recentf
  :straight (:type built-in)
  :hook (after-init . recentf-mode)
  :init
  (setq recentf-max-saved-items 200
        recentf-auto-cleanup 'never) ;; disable before we start recentf!
  :config
  (recentf-mode 1)
  :custom
  (recentf-save-file (expand-file-name "recentf" user-emacs-directory))
  (recentf-keep '(file-remote-p file-readable-p))
  (recentf-exclude '((expand-file-name package-user-dir)
                     ".mp4"
                     "/ssh.*"
                     ".cache"
                     ".cask"
                     ".elfeed"
                     "bookmarks"
                     "cache"
                     "ido.*"
                     "persp-confs"
                     "recentf"
                     "undo-tree-hist"
                     "url"
                     "COMMIT_EDITMSG\\'")))

;; Do not saves duplicates in kill-ring
(customize-set-variable 'kill-do-not-save-duplicates t)

;; Make scrolling less stuttered
(setq auto-window-vscroll nil)
(customize-set-variable 'fast-but-imprecise-scrolling t)
(customize-set-variable 'scroll-conservatively 101)
(customize-set-variable 'scroll-margin 5)
(customize-set-variable 'scroll-preserve-screen-position t)

;; Middle-click paste at point, not at cursor.
(setq mouse-yank-at-point t)

;; Better support for files with long lines
;; Handle long lines in files
(use-package so-long
  :init
  (setq so-long-threshold 400) ; reduce false positives w/ larger threshold
  (global-so-long-mode 1)
  :config
  (delq! 'font-lock-mode so-long-minor-modes)
  (delq! 'display-line-numbers-mode so-long-minor-modes)
  (delq! 'buffer-read-only so-long-variable-overrides 'assq)
  (add-to-list 'so-long-variable-overrides '(font-lock-maximum-decoration . 1))
  (add-to-list 'so-long-variable-overrides '(save-place-alist . nil))
  (appendq! so-long-minor-modes
            '(flycheck-mode
              spell-fu-mode
              eldoc-mode
              smartparens-mode
              highlight-numbers-mode
              better-jumper-local-mode
              ws-butler-mode
              auto-composition-mode
              undo-tree-mode
              highlight-indent-guides-mode
              hl-fill-column-mode))
  (setq so-long-predicate (lambda ()
                            (unless (bound-and-true-p visual-line-mode)
                              (let ((so-long-skip-leading-comments
                                     ;; HACK Fix #2183: `so-long-detected-long-line-p' tries to parse
                                     ;;      comment syntax, but comment state may not be initialized,
                                     ;;      leading to a wrong-type-argument: stringp error.
                                     (bound-and-true-p comment-use-syntax)))
                                (so-long-detected-long-line-p))))))

;; Handle large files
(use-package vlf)

;; Make shebang (#!) file executable when saved
(add-hook 'after-save-hook #'executable-make-buffer-file-executable-if-script-p)

;; Enable savehist-mode for an command history
(use-package savehist
  :straight (:type built-in)
  :init (savehist-mode 1)
  :custom
  (savehist-file (expand-file-name "history" user-emacs-directory)))


;; Open files in last editing place
(use-package saveplace
  :straight (:type built-in)
  :init
  (save-place-mode 1)
  :config
  (setq save-place-forget-unreadable-files nil))

;; User profile
(customize-set-variable 'user-full-name "David Delarosa")
(customize-set-variable 'user-mail-address "xdavidel@gmail.com")

(use-package hexl
  :straight (:type built-in)
  :init
  ;; Auto open binary files with `hexl-mode' if no other mode deteced.
  (add-to-list 'magic-fallback-mode-alist '(buffer-binary-p . hexl-mode) t)
  :config
  (setq hexl-bits 8))

(provide 'm-defaults)
;;; m-defaults.el ends here
