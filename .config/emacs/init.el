;;; init.el --- Emacs main configuration file -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; Loading eraly-init.el if Emacs version < 27
(unless (featurep 'early-init)
  (load (expand-file-name "early-init" user-emacs-directory)))

;; Consts
(defconst IS-MAC          (eq system-type 'darwin)
  "Does Emacs runs on a macos system.")
(defconst IS-LINUX        (eq system-type 'gnu/linux)
  "Does Emacs runs on a linux based operating system.")
(defconst IS-WINDOWS      (memq system-type '(cygwin windows-nt ms-dos))
  "Does Emacs runs on a windows based system.")
(defconst IS-BSD          (or IS-MAC (eq system-type 'berkeley-unix))
  "Does Emacs runs on BSD based system.")

(defun termux-p ()
  "Check if Emacs is running under Termux."
  (string-match-p
   (regexp-quote "/com.termux/")
   (expand-file-name "~")))

;; Package Management
;; ------------------------------------
;; Use Straight el
(setq straight-check-for-modifications '(check-on-save find-when-checking))
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; Install use-package
(straight-use-package 'use-package)
(setq use-package-verbose nil ; don't print anything
      use-package-compute-statistics nil; compute statistics about package initialization
      use-package-expand-minimally t ; minimal expanded macro
      use-package-always-defer t) ; always defer, don't "require", except when :demand

;; Makes :straight t by default
(setq straight-use-package-by-default t)


;;; Prevent builtin Org from being loaded
(straight-register-package 'org)
(straight-register-package 'org-contrib)

(customize-set-variable 'custom-file
                        (expand-file-name "custom.el" user-emacs-directory-orig))

;; Add the modules folder to the load path
(add-to-list 'load-path (expand-file-name "modules/" user-emacs-directory-orig))


(defcustom emacs-leader-key " "
  "The key to use as a leader key.")

;; general for kybinding
(use-package general
  :demand
  :config
  (general-define-key "C-c ?" 'general-describe-keybindings))

(use-package diminish
  :demand)

;; Load modules here
(require 'm-helpers)
(require 'm-defaults)
(require 'm-screencast)
(require 'm-ui)
(require 'm-editing)
(require 'm-evil)
(require 'm-completion)
(require 'm-project)
(require 'm-org)
(require 'm-lsp)
(require 'm-keybinds)
(require 'm-tools)
(require 'm-shell)
(require 'm-remote)
(require 'm-modes)

;; ------------------------------------

;; Enable server
(use-package server
  :straight (:type built-in)
  :config
  (or (server-running-p)
      (server-start)))

(provide 'init)
;;; init.el ends here
