#!/bin/sh
# Profile file. Runs on login.

# Adds `~/.scripts` and all subdirectories to $PATH
export PATH="$PATH:$HOME/.local/bin:$(du "$HOME/.local/scripts" | cut -f2 | paste -sd ':' -)"
export EDITOR="nvim"
export EDITORGUI="nvim"
export TERMINAL="st"
export TERMINAL_SEC="alacritty"
export BROWSER="brave"
export BROWSERP="brave --incognito"
export READER="zathura"
export FILE="vifm"
export FILEGUI="pcmanfm"
export MEDIAPLAYER="mpv"

export _JAVA_AWT_WM_NONREPARENTING=1	# Fix for Java applications in dwm
export QT_QPA_PLATFORMTHEME="gtk2"	    # Have QT use gtk2 theme.
export MOZ_USE_XINPUT2="1"		        # Mozilla smooth scrolling/touchpads.

# No place like home
# ================================================================
export LESSHISTFILE="-"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_DATA_DIRS="/usr/local/share:/usr/share"

export XAUTHORITY="${XDG_RUNTIME_DIR:-/run/user/$UID}/Xauthority"
export SUDO_ASKPASS="$HOME/.local/scripts/dmenupass"

export ANDROID_SDK_HOME="$XDG_CONFIG_HOME/android"
export DOCKER_CONFIG="$XDG_CONFIG_HOME/docker"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc-2.0"
export INPUTRC="$XDG_CONFIG_HOME/inputrc"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export PYTHONSTARTUP="$XDG_CONFIG_HOME/python/pythonrc"
export WEECHAT_HOME="$XDG_CONFIG_HOME/weechat"
export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
export XINITRC="$XDG_CONFIG_HOME/x11/xinitrc"
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"

export CARGO_HOME="$XDG_DATA_HOME/cargo"
export GOPATH="$XDG_DATA_HOME/go"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export WINEPREFIX="$XDG_DATA_HOME/wineprefixes/default"

export EMACSNATIVELOADPATH="$XDG_CACHE_HOME/emacs"
export HISTFILE="$XDG_CACHE_HOME/shell_history"
export PYLINTHOME="$XDG_CACHE_HOME/pylint"
# ================================================================

# MPD
export MPD_HOST="127.0.0.1"
export MPD_PORT="6600"

export LOCATION="Jerusalem"

# Dotfiles
export DOTCONFDIR="${HOME}/.local/dotfiles"

# less/man colors
export LESS=-R
export LESS_TERMCAP_mb="$(printf '%b' '')"; a="${a%_}"
export LESS_TERMCAP_md="$(printf '%b' '')"; a="${a%_}"
export LESS_TERMCAP_me="$(printf '%b' '')"; a="${a%_}"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"; a="${a%_}"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"; a="${a%_}"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"; a="${a%_}"
export LESSOPEN="| /usr/bin/highlight -O ansi %s 2>/dev/null"

# Start ssh-agent if not aleady running
if [ -n "$XDG_RUNTIME_DIR" ]; then
  ! pidof ssh-agent > /dev/null && \
    ssh-agent > "$XDG_RUNTIME_DIR/ssh-agent.env" 2>/dev/null

# Source cached ssh output
[ -z "$SSH_AUTH_SOCK" ] && \
  source "$XDG_RUNTIME_DIR/ssh-agent.env" >/dev/null 2>&1
fi

# Start graphical server if not already running.
[ "$(tty)" = "/dev/tty1" ] && \
    command -v startx && \
    ! pidof -s Xorg >/dev/null 2>&1 && \
    exec startx $XINITRC
